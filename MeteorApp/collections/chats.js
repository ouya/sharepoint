import {Mongo} from 'meteor/mongo';

Chats = new Mongo.Collection('chats');

export default Chats;
