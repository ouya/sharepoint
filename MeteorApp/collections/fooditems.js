import {Mongo} from 'meteor/mongo';

Fooditems = new Mongo.Collection('fooditems');

export default Fooditems;
