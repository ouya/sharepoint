Meteor.methods({
    // PUSH NOTIFICATIONS
    'updateDeviceToken': function(userId, token){
        console.log("userId: " + userId);
        console.log("token: " + token);
        Meteor.users.update({ _id: userId }, { $set: { 'profile.token': token }});
    },
    'addChatMessage': function(messages, fooditemId) {
        var returnValue = Chats.insert({ chatData: messages, fooditem: fooditemId});

        var chats = Chats.find({fooditem: fooditemId});
        var usersGivenNot = [];
        chats.forEach(function(chat){
          var userId = chat.chatData[0].user._id;
          if (usersGivenNot.indexOf(userId) < 0) {
            usersGivenNot.push(userId);

            var token = Meteor.users.findOne({ _id: userId }).profile.token;
            var title = Fooditems.findOne({_id: fooditemId}).itemName;

            // notify users in chat group
            Meteor.call('chatNotification', token, title, messages, function(error, result) {
                console.log(error);
                console.log(result);
                return true;
            })
          }
        });
    },
    'newItemBroadcast': function(fooditemId, range) {
      // TODO retrieve current location of all users and send to all users within range
    },
    //NORMAL
    'addActiveChat': function(fooditemId, userId) {
      var activeChats = Meteor.users.findOne({_id : userId}).profile.activeChats;
      // avoid duplicate chatGroup entries
      if (activeChats.indexOf(fooditemId) === -1) {
        activeChats.push(fooditemId);
        Meteor.users.update({ _id: Meteor.userId() }, { $set: { 'profile.activeChats': activeChats }});
        // Meteor.users.update({ _id: Meteor.userId() }, { $inc: { 'profile.points': 1 }});
      }
    },
    'imageUpload': function(myGraphic){
        return Foodimages.insert({"image": myGraphic});
    },
    'foodItemHasChats': function(fooditemId) {
      return (Chats.find({fooditem: fooditemId}).count() > 0);
    },
    'checkRewards': function(userId, before, after){
      if (before < 10 && after >= 10) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 20 && after >= 20) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 30 && after >= 30) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 40 && after >= 40) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 50 && after >= 50) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 60 && after >= 60) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 70 && after >= 70) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 80 && after >= 80) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }
      if (before < 90 && after >= 90) {
        Meteor.users.update({ _id: userId }, { $inc: { 'profile.rewardlevel': 1 }});
      }

//       How many levels - 10 levels.
// 1st level 0 - 29 points (30 point increments)
// 2nd level 30 - 59 points (30 point increments)
// 3rd level 60 - 99 points (40 point increments)
// 4th level 100 - 139 points (40 point increments)
// 5th level 140 - 189 points (50 point increments)
// 6th level 190 - 239 points (50 point increments)
// 7th level 240 - 299 points (60 point increments)
// 8th level 300 - 359 points (60 point increments)

    },
    'addPoints': function(userId, points) {
      var before = Meteor.users.findOne({ _id: userId }).profile.points;
      Meteor.users.update({ _id: userId }, { $inc: { 'profile.points': points }});

      Meteor.call('checkRewards', userId, before, before + points);
    },
    'addPointsForChatPersons': function(foodItemId, points) {
      // search all chats where this foodItem is found
      //- then give points to the people in the chatroom including provider here as well
      var chats = Chats.find({fooditem: foodItemId});
      var usersAlreadyGivenPoints = [];
      chats.forEach(function(chat){
        var userId = chat.chatData[0].user._id;
        if (usersAlreadyGivenPoints.indexOf(userId) < 0) {
          usersAlreadyGivenPoints.push(userId);
          // not found in user list - give points
          var before = Meteor.users.findOne({ _id: userId }).profile.points;
          Meteor.users.update({ _id: userId }, { $inc: { 'profile.points': points }});
          Meteor.call('checkRewards', userId, before, before + points);
        }
      });
    },
    'deleteFooditem': function(foodItemId) {
      // 0. Remove image for foodItem
      Foodimages.remove(Fooditems.findOne({_id: foodItemId}).imageId);

      // 1. Remove chats which correspond to this foodItemId
      Chats.remove({ fooditem: fooditemId});

      // 2. Active Chats - a user state - must be updated too
      var users = Meteor.users.find();
      users.forEach(function(user){
        // check all users
        // fooditem listed in some chats user took part?
        var activeChats = user.profile.activeChats;
        var index = activeChats.indexOf(foodItemId);
        if (index > -1)
          activeChats.splice(index, 1);

        Meteor.users.update({ _id: user._id }, { $set: { 'profile.activeChats': activeChats }});
      });

      // 3. Remove foodItem
      Fooditems.remove(foodItemId);
    }
})

// Meteor.users.allow({
//   update: function(userId, user) {
//     return true;
//
//     /**
//      * Don't use `return true` in production!
//      * You probably need something like this:
//      * return Meteor.users.findOne(userId).profile.isAdmin;
//      */
//   }
// });
Accounts.onLogin(function(user){
 console.log(user)

 // on login we want the new device token to be used (for push notification)
 // so on every new device login we have a new token
 // user.profile.token = options.token;
});


Accounts.onCreateUser(function(options, user) {
  console.log(options);
  // Use provided profile in options, or create an empty object
  user.profile = options.profile || {};
  // // Assigns first and last names to the newly created user object
  user.profile.firstName = options.profileusername;
  // user.profile.lastName = options.lastName;
  //
  user.profile.activeChats = [];
  user.profile.rewards = [];
  user.profile.rewards.push("none");
  user.profile.rewardlevel = 0;
  user.profile.points = 0;
  user.profile.token = options.token;
  user.profile.name = user.username;

  return user;
});
