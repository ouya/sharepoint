import { Meteor } from 'meteor/meteor';

Meteor.publish('chats', () => {
  return Chats.find();
});

Meteor.publish('fooditems', () => {
  return Fooditems.find();
});

Meteor.publish('fooditemsById', (id) => {
  return Fooditems.find({_id : id});
});

Meteor.publish('userfooditems', (userId) => {
  return Fooditems.find({user : userId});
});

Meteor.publish('foodimages', (id) => {
  return Foodimages.find({_id : id});
});

Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.publishComposite('foodWithImage', function(chatGroupId) {
    return {
        find: function() {
            return Fooditems.find({ _id: chatGroupId});
        },
        children: [
          {
            find: function(fooditem) {
                return Foodimages.find({ _id: fooditem.imageId });
            }
          }
        ]
    }
});

Meteor.publishComposite('myposts', function(userId) {
    return {
        find: function() {
            return Fooditems.find({user : userId});
        },
        children: [
          {
            find: function(fooditem) {
                return Foodimages.find({ _id: fooditem.imageId });
            }
          }
        ]
    }
});


Meteor.publish("users", function () {
  return Meteor.users.find({}, {fields: {username: 1, emails: 1, profile: 1}});
});
