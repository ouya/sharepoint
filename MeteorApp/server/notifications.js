import {HTTP} from 'meteor/http'

Meteor.methods({
    //   application/json for JSON
    sendNot: function() {
          var result = HTTP.call('POST', 'https://fcm.googleapis.com/fcm/send', {
            data:
            {
                    "notification": {
                        "title": "Your Title",
                        "text": "Your Text",
                        "icon": "ic_launcher",
                        "click_action": "fcm.ACTION.HELLO" // should match to your intent filter
                    },
                    "to": "/topics/fmf-dev",
                    "time_to_live": 30,
                    "data": {
                        "message": "developers' test message!"
                    }
            },
            headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'key=AAAATWcteIY:APA91bHzLH5XxYsFEP9AVznOuZz0fXx70bcrRl8cNnHr9_RWB4Ts7b2VOv7GdneLk61dGd6eKHxXU1HYFXSn28mYqdE70d5-Ll28pa0ch12hG4ToGoBkRFiavyUjZ8iCJQBn8QPRYk1sBYRI5aH47LJ1sh4Sto1PDA'
            }
        }
    );
      console.log(result);
    },
    chatNotification: function(token, topic, message) {
          var result = HTTP.call('POST', 'https://fcm.googleapis.com/fcm/send', {
            data:
            {
                    "notification": {
                        "title": "New message: " + topic,
                        "text": message[0].text,
                        "icon": "ic_notification",
                        "click_action": "fcm.ACTION.HELLO", // should match to your intent filter
                        "show_in_foreground": true,
                        "sound": "apple.mp3"
                    },
                    "to": token,
                    "time_to_live": 30,
                    "data": {
                        "message": message
                    }
            },
            headers: {
                    'Content-type': 'application/json',
                    'Authorization': 'key=AAAATWcteIY:APA91bHzLH5XxYsFEP9AVznOuZz0fXx70bcrRl8cNnHr9_RWB4Ts7b2VOv7GdneLk61dGd6eKHxXU1HYFXSn28mYqdE70d5-Ll28pa0ch12hG4ToGoBkRFiavyUjZ8iCJQBn8QPRYk1sBYRI5aH47LJ1sh4Sto1PDA'
            }
        }
    );
      console.log(result);
    },



    });
