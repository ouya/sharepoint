module.exports = {
  servers: {
    one: {
      host: '46.101.176.109',
      username: 'root',
      password: "kYozjzQLYp%X@S%f",
      opts: {
          port: 22,
      },
    }
  },
  // Show a progress bar during the upload of the bundle to the server.
   // Might cause an error in some rare cases if set to true, for instance in Shippable CI
  enableUploadProgressBar: true,

   // Application name (no spaces).
  appName: "feedmyfriends",
  meteor: {
    name: 'feedmyfriends',
    path: '../',
    dockerImage: 'abernix/meteord:base',
    servers: {
      one: {},// list of servers to deploy, from the 'servers' list
    },
    buildOptions: {
      serverOnly: true,
      debug: true,
      cleanAfterBuild: true // default
    },
    env: {
      ROOT_URL: 'http://46.101.176.109',
      MONGO_URL: 'mongodb://localhost/meteor'
    },
    deployCheckWaitTime: 180 //default 10
  },
  mongo: { // <NOT>optional</NOT>
    oplog: true,
    port: 27017,
    servers: {
      one: {},
    },
  },
};
