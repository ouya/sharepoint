import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  View,
  StatusBar
} from 'react-native';
//new router import
import {
    StackNavigation,
    TabNavigation,
    TabNavigationItem as TabItem,
   } from '@exponent/ex-navigation';

import { Router } from './Router';
import Icon from 'react-native-vector-icons/Ionicons';
import { COLORS , STYLES } from './styles';

var myParam; //TODO name it
var myChatGroup;

type TabRenderFunction = (isSelected: bool) => ReactElement<any>;

export default class TabNavigationLayout extends React.Component {

  static route = {
    navigationBar: {
      visible: false,
    }
  }

  render() {
    StatusBar.setHidden(true);
    return (
      <TabNavigation
        tabBarColor={COLORS.tabBar}
        tabBarHeight={56}
        navigatorUID="main"
        initialTab="profile">
        <TabItem
          id="add"
          selectedStyle={styles.selectedTab}
          renderIcon={isSelected => this._renderIcon('Add new Item', 'md-add-circle', isSelected)}>
          <StackNavigation
            id="add"
            defaultRouteConfig={{
              navigationBar: {
                visible: true,
                title: "Add new Item",
                tintColor: COLORS.navigationBarTintColor,
                backgroundColor: COLORS.navigationBarBackgroundColor,
              }
            }}
            initialRoute={Router.getRoute('itemcamera')}
          />
        </TabItem>
        <TabItem
          id="map"
          selectedStyle={styles.selectedTab}
          renderIcon={isSelected => this._renderIcon('Map', 'md-map', isSelected)}>
          <StackNavigation
            id="map"
            defaultRouteConfig={{
              navigationBar: {
                visible: true,
                title: "Map",
                tintColor: COLORS.navigationBarTintColor,
                backgroundColor: COLORS.navigationBarBackgroundColor,
              }
            }}
            initialRoute={Router.getRoute('map')}
          />
        </TabItem>
        <TabItem
          id="contacts"
          selectedStyle={styles.selectedTab}
          renderIcon={isSelected => this._renderIcon('Contacts', 'md-chatbubbles', isSelected)}>
          <StackNavigation
            id="contacts"
            defaultRouteConfig={{
              navigationBar: {
                visible: true,
                title: "Contacts",
                tintColor: COLORS.navigationBarTintColor,
                backgroundColor: COLORS.navigationBarBackgroundColor,
              }
            }}
            initialRoute={Router.getRoute('contacts')}
          />
        </TabItem>
        <TabItem
          id="profile"
          selectedStyle={styles.selectedTab}
          renderIcon={isSelected => this._renderIcon('Profile', 'md-contact', isSelected)}>
          <StackNavigation
            id="profile"
            defaultRouteConfig={{
              navigationBar: {
                visible: true,
                title: "Profile",
                tintColor: COLORS.navigationBarTintColor,
                backgroundColor: COLORS.navigationBarBackgroundColor,
              }
            }}
            initialRoute={Router.getRoute('profile')}
          />
        </TabItem>
{/*
        <TabItem
          id="contacts"
          title="Contacts"
          selectedStyle={styles.selectedTab}
          renderIcon={isSelected => this._renderIcon('Contacts', 'ios-compass-outline', isSelected)}>
          <StackNavigation
            id="contacts"
            defaultRouteConfig={defaultRouteConfig}
            initialRoute={Router.getRoute('contactsRoute')}
          />
        </TabItem> */}
      </TabNavigation>
    );
  }

  _renderIcon(title: string, iconName: string, isSelected: bool): ReactElement<any> {
    let color = isSelected ? COLORS.tabIconSelected : COLORS.tabIconDefault;

    return (
      <View style={styles.tabItemContainer}>
        <Icon name={iconName} size={48} color={color} />

        {/* <Text style={[styles.tabTitleText, {color}]} numberOfLines={1}>
          {title}
        </Text> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tabItemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitleText: {
    fontSize: 11,
  },
  selectedTab:{
    backgroundColor: COLORS.tabBackgroundSelected,
  },
});
