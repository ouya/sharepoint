import React from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import {
  NavigationProvider,
  StackNavigation
} from '@exponent/ex-navigation';
import { Router } from './Router';

import Loading from './components/Loading';
import config from './config';

import FCM from "react-native-fcm";
import firebaseClient from  "./FirebaseClient";

Meteor.connect(config.METEOR_URL);

const RNApp = (props) => {
  const { status, user, loggingIn } = props;
  if (status.connected === false || loggingIn) {
    return <Loading />;
  } else if (user) {
      return (
      <NavigationProvider router={Router}>
      <StackNavigation initialRoute={Router.getRoute('tabnav')} />
      </NavigationProvider>
    );
  } else {
    return (
        <NavigationProvider router={Router}>
        <StackNavigation initialRoute={Router.getRoute('signin')} />
        </NavigationProvider>
    );
  }
};

RNApp.propTypes = {
  status: React.PropTypes.object,
  user: React.PropTypes.object,
  loggingIn: React.PropTypes.bool,
};

export default createContainer(() => {
  return {
    status: Meteor.status(),
    user: Meteor.user(),
    loggingIn: Meteor.loggingIn(),
  };
}, RNApp);
