import { COLORS } from '../styles';

var primary = COLORS.primary;		//theme background
var secondary = COLORS.secondary;	//header
var info = COLORS.info;
var success = COLORS.success;
var danger = COLORS.danger;
var warning = COLORS.warning;
var sidebar = COLORS.sidebar;
var dark = COLORS.dark;
var light = COLORS.light;

// var darken = secondary.darken(0.2);

module.exports = {
	brandPrimary : primary,
	brandSecondary: secondary,
	brandInfo: info,
	brandSuccess: success,
	brandDanger: danger,
	brandWarning: warning,
	brandSidebar: sidebar,
	// darker: darken,
	dark: dark,
	light: light
}
