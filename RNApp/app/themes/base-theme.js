//import Color from 'color';
import { COLORS } from '../styles';

import { Platform } from 'react-native';
var primary = require('./variable').brandPrimary;
var secondary = require('./variable').brandSecondary;
var info = require('./variable').brandInfo;
var success = require('./variable').brandSuccess;
var danger = require('./variable').brandDanger;
var warning = require('./variable').brandWarning;
var sidebar = require('./variable').brandSidebar;
module.exports = {
    brandPrimary : primary,
    brandInfo: info,
    brandSuccess: success,
    brandDanger: danger,
    brandWarning: warning,
    brandSidebar: sidebar,
    brandSecondary: secondary,   //new style

    inverseTextColor: "#fff",
    textColor: "#333",

    fontSizeBase: 15,
    titleFontSize: 18,

    //TABS in Contacts
    tabBgColor: COLORS.tabBgColor,
    tabTextColor: COLORS.tabTextColor,

    get darkenHeader() {
        return COLORS.tabBgColor;
    },

    get fontSizeH1 () {
        return this.fontSizeBase*1.8;
    },
    get fontSizeH2 () {
        return this.fontSizeBase* 1.6;
    },
    get fontSizeH3 () {
        return this.fontSizeBase* 1.4;
    },
    get btnTextSize () {
        return this.fontSizeBase* 1.1;
    },
    get btnTextSizeLarge () {
        return this.fontSizeBase* 1.5;
    },
    get btnTextSizeSmall () {
        return this.fontSizeBase* .8;
    },
    get iconSizeLarge () {
        return this.iconFontSize* 1.4;
    },
    get iconSizeSmall () {
        return this.iconFontSize* .6;
    },

    borderRadiusBase: 4,

    get borderRadiusLarge () {
        return this.fontSizeBase* 3.8;
    },

    footerHeight: 35,
    toolbarHeight: (Platform.OS === 'ios' ) ? 70 : 55,
    toolbarDefaultBg: secondary,
    toolbarInverseBg: "#fff",



    get btnPrimaryBg () {
        return this.brandPrimary;
    },
    get btnPrimaryColor () {
        return this.inverseTextColor;
    },
    get btnSuccessBg () {
        return this.brandSuccess;
    },
    get btnSuccessColor () {
        return this.inverseTextColor;
    },
    get btnDangerBg () {
        return this.brandDanger;
    },
    get btnDangerColor () {
        return this.inverseTextColor;
    },
    get btnInfoBg () {
        return this.brandInfo;
    },
    get btnInfoColor () {
        return this.inverseTextColor;
    },
    get btnWarningBg () {
        return this.brandWarning;
    },
    get btnWarningColor () {
        return this.inverseTextColor;
    },

    borderWidth: 1,

    get inputColor () {
        return this.textColor;
    },
    get inputColorPlaceholder () {
        return 'rgba(255, 255, 255, 1.0)';
    },

    inputBorderColor: "#fff",
    inputHeightBase: 50,
    inputGroupMarginBottom: 10,
    inputPaddingLeft: 5,
    get inputPaddingLeftIcon () {
        return this.inputPaddingLeft* 8;
    },

        // Radio Button
        radioBtnSize: (Platform.OS === 'ios') ? 25 : 23,
        radioColor: '#7e7e7e',

        get radioSelectedColor() {
            return this.brandPrimary;
        },

    dropdownBg: "#fff",
    dropdownLinkColor: "#414142",

    jumbotronPadding: 30,
    jumbotronBg: "#C9C9CE",

    contentPadding: 10,

    listBorderColor: "rgba(181, 181, 181, 0.34)",
    listDividerBg: "#f2f2f2",
    listItemPadding: 15,
    listNoteColor: "#ddd",
    listBg: "#fff",

    // Icon
    iconFamily: 'Ionicons',
    iconFontSize: (Platform.OS === 'ios' ) ? 30 : 28,
    iconMargin: 7,

    badgeColor: "#fff",
    badgeBg: "#ED1727",

    // Line Height
    btnLineHeight: 19,
    lineHeightH1: 32,
    lineHeightH2: 27,
    lineHeightH3: 22,
    iconLineHeight: (Platform.OS === 'ios' ) ? 37 : 30,
    lineHeight: (Platform.OS === 'ios' ) ? 20 : 24,


    defaultSpinnerColor: "#ffffff",
    inverseSpinnerColor: "#ffffff",

    defaultProgressColor: "#E4202D",
    inverseProgressColor: "#ffffff"
}
