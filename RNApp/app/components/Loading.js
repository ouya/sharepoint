import React from 'react';
import {
  View,
  StyleSheet,
  Platform,
  ActivityIndicator,
  ActivityIndicatorIOS,
} from 'react-native';
import { COLORS } from '../styles';

import { Container, Content } from 'native-base';
import theme from '../themes/base-theme';
const styles = StyleSheet.create({
});

const IOSLoading = (props) => (
  <ActivityIndicatorIOS
    animating
    size={props.size}
    {...props}
  />
);

const AndroidLoading = (props) => (
  <ActivityIndicator
    style={{
      height: props.size === 'large' ? 40 : 20,
    }}
    styleAttr="Inverse"
    {...props}
  />
);

const Loading = (props) => {
  let LoadingComponent = AndroidLoading;

  if (Platform.OS === 'ios') {
    LoadingComponent = IOSLoading;
  }

  return (
    <Container>
    <Content theme={theme}>
      <LoadingComponent {...props} />
    </Content>
    </Container>
  );
};

Loading.propTypes = IOSLoading.propTypes = AndroidLoading.propTypes = {
  size: React.PropTypes.string,
};

Loading.defaultProps = {
  size: 'large',
};

export default Loading;
