import React from 'react';
import {View, Text, Image, StyleSheet } from 'react-native';
import gravatar from 'gravatar-api';
import placeholderImage from './placeholder.png';
import { COLORS , STYLES } from '../../styles';

const Avatar = (props) => {
  var size = props.size;
  var styles = StyleSheet.create({
    avatartext:{
    fontSize: size,
    fontWeight: "bold",
    color: COLORS.headerText,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  circle: {
      width: size*1.7,
      height: size*1.7,
      borderRadius: size*1.7/2,
      backgroundColor: COLORS.avatarBackground,
      alignItems:'center',
      justifyContent:'center',
  }
  });
  var username = props.username.substring(0,2).toUpperCase();
  return (
    <View style={styles.circle}>
    <Text style={styles.avatartext}>
    {username}
    </Text>
    </View>
  );
};

Avatar.propTypes = {
  username: React.PropTypes.string,
  size: React.PropTypes.number,
};

export default Avatar;
