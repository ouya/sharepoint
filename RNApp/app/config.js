// If you're running on a device or in the Android simulator be sure to change

// localhost
//let METEOR_URL = 'ws://localhost:3000/websocket';

//for production server
let METEOR_URL = 'ws://46.101.176.109/websocket';
// if (process.env.NODE_ENV === 'production') {
//   METEOR_URL = 'ws://46.101.176.109'; // your production server url
// }

const config = {
  env: process.env.NODE_ENV,
  METEOR_URL,
};

console.disableYellowBox = true;

export default config;
