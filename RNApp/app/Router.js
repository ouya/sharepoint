import {
  createRouter,
} from '@exponent/ex-navigation';

import TabNavigationLayout from './TabNavigationLayout';

import Profile from './routes/Profile';
import Map from './routes/Map';
import Contacts from './routes/Contacts';
import MyPosts from './routes/Profile/MyPosts';
import MyRewards from './routes/Profile/MyRewards';

import ItemCamera from './routes/ItemWizard/ItemCamera';
import ItemLocation from './routes/ItemWizard/ItemLocation';
import ItemDetails from './routes/ItemWizard/ItemDetails';

import MarkerDetails from './routes/MarkerDetails';
import Chat from './routes/Chat';
import SignIn from './routes/SignIn';
import PushTest from './routes/PushTest';
import ShowReward from './routes/Profile/ShowReward';

export const Router = createRouter(() => ({
  signin: () => SignIn,
  tabnav: () => TabNavigationLayout,

  profile: () => Profile,
  myposts: () => MyPosts,
  myrewards: () => MyRewards,

  map: () => Map,
  contacts: () => Contacts,

  itemcamera: () => ItemCamera,
  itemlocation: () => ItemLocation,
  itemdetails: () => ItemDetails,

  markerdetails: () => MarkerDetails,
  chat: () => Chat,

  showreward: () => ShowReward,
  pushTest: () => PushTest

  }

));
