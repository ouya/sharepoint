import React, { Component } from 'react';
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { COLORS } from '../../styles';
import Logo from '../../images/Logo.png';
import theme from '../../themes/base-theme';
import { LayoutAnimation } from 'react-native';
import Meteor, { Accounts } from 'react-native-meteor';
import { Router } from '../../Router';
import { Modal, TouchableHighlight } from 'react-native';
import { Button, Container, Content, Card, CardItem, Thumbnail, List, ListItem, InputGroup, Input, Text, Icon} from 'native-base';

import GeossLogo from '../../images/logo_mygeosss.png';

import PushController from "../../PushController";
import firebaseClient from  "../../FirebaseClient";


const window = Dimensions.get('window');

const styles = StyleSheet.create({
 background:{
 padding: 30,
 backgroundColor: COLORS.primary,
 },
 button: {
 paddingLeft: 10,
 paddingRight: 10,
 marginRight: 10,
 },
 buttontext:{
 color: COLORS.info,
 fontWeight: "600",
 },
 buttonscontainer: {
 flexDirection: 'row',
 justifyContent: 'center',
 },
 listicon:{
 color: COLORS.info,
 fontSize: 20,
 },
 privacymaincontainer:{
   flex: 0,
   zIndex: 5,
   flexDirection: 'column',
   position: 'absolute',
   height: window.height -20,
   top: 0,
   left: 0,
 },
 privacycontainer: {
 flexDirection: 'row',
 justifyContent: 'flex-start',
 marginTop: -6,
 marginLeft: 3,
 height: 40,
 marginBottom: -10,
 },
 privacybuttons:{
 marginRight: 7,
 marginLeft: 7,
 paddingLeft: 10,
 paddingRight: 10,
 },
 privacyicon: {
 color: COLORS.info,
 fontSize: 20,
 marginTop: 20,
 marginRight: 8,
 },
 privacytext: {
 color: COLORS.info,
 fontSize: 14,
 },
 privacynotetext:{
 fontSize: 10,
 alignSelf: 'center',
 },
 error: {
   marginTop: 10,
marginBottom: 10,
padding: 5,
 height: 30,
 justifyContent: 'center',
 width: window.width,
 alignSelf: 'center',
 alignItems: 'center',
 backgroundColor: COLORS.danger,
 },
 noerror: {
marginTop: 5,
marginBottom: 5,
padding: 5,
 height: 30,
 justifyContent: 'center',
 width: window.width,
 alignSelf: 'center',
 alignItems: 'center',
 },
 errorText: {
 color: COLORS.sidebar,
 fontSize: 16,
 fontWeight:'bold',
 marginTop: 10,
 marginBottom:10,
 },
 logo: {
 alignSelf: 'center',
 width: 200,
 height: 200,
 },
 subHeaderText: {
 alignSelf: 'center',
 marginBottom: 10,
 marginTop: 10,
 fontSize: 22,
 color: COLORS.headerText,
 fontWeight: '600',
 fontStyle: 'italic',
 },

});

class SignIn extends Component {
 constructor(props) {
 super(props);

 this.state = {
  email: '',
  password: '',
  confirmPassword: '',
  confirmPasswordVisible: false,
  error: null,
  legal: false,
  token: ""
 };

 this.updateState = this.setState.bind(this);
 this.createAccount= this.handleCreateAccount.bind(this);
 this.signIn = this.handleSignIn.bind(this);
 this.openLegal = this.openLegal.bind(this);
 this.showConfirmPassword = this.showConfirmPassword.bind(this);
 this.accept = this.accept.bind(this);
 this.decline = this.decline.bind(this);
 }

 accept() {
this.setState({ legal: false });
 this.handleCreateAccount();
 }

 decline() {
 // reset to login if you decline privacy statements
 this.setState({ legal: false });
 this.setState({ error: 'Privacy needs to be accepted.' });
 }


 validInput(overrideConfirm) {
  const { email, password, confirmPassword, confirmPasswordVisible } = this.state;
  let valid = true;

  if (email.length === 0 || password.length === 0) {
  this.setState({ error: 'Email and password cannot be empty.' });
  valid = false;
  }

  if (!overrideConfirm && confirmPasswordVisible && password !== confirmPassword) {
  this.setState({ error: 'Passwords do not match.' });
  valid = false;
  }

  if (valid) {
  this.setState({ error: null });
  }
  return valid;
 }

 handleSignIn() {
  this.setState({ confirmPasswordVisible: false});

  if (this.validInput(true)) {
  const { email, password, token } = this.state;

  Meteor.loginWithPassword(email, password, token,  (err) => {
     if (err) {
     this.setState({ error: err.reason });
     }
  });
  }

 }

 showConfirmPassword() {
  LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
  this.setState({ confirmPasswordVisible: true});
 }

 handleCreateAccount() {
  const { email, password, confirmPasswordVisible, token } = this.state;

  if (confirmPasswordVisible && this.validInput()) {
    var username = email.substring(0,email.indexOf('@'));
    Accounts.createUser({ email, username, password, token }, (err) => {
     if (err) {
     this.setState({ error: err.reason });
     } else {
       // hack because react-native-meteor doesn't login right away after sign in
       this.handleSignIn();
     }
    });
  }
 }

 openLegal() {
  this.setState({ legal: true });
 }
 render() {
   return (
     <Container>
     {this.state.legal ? (
       <View style={styles.privacymaincontainer}>
       <Content theme={theme}>
       <Card>
       <CardItem>
       <Text>Privacy Agreement</Text>
       </CardItem>
       <CardItem>
       <Image
       style={styles.logo}
       source={GeossLogo}
       />
       </CardItem>
       <CardItem>
       <Text>
       This application has been developed within the MYGEOSS project, which has received funding from the European Union’s Horizon 2020 research and innovation programme. The JRC, or as the case may be the European Commission, shall not be held liable for any direct or indirect, incidental, consequential or other damages, including but not limitedto the loss of data, loss of profits, or other financial loss arising from the use of this application, or inability to use it, even if the JRC is notified of the possibility of such damages.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       All personal information displayed in the Feed my Friends App or other MYGEOSS applications is
       subject to the following Specific Privacy Statement, accepted by the data subject prior to the
       collection of his/her personal data, and is published with his/her unambiguous consent.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       SPECIFIC PRIVACY STATEMENT
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       1. Description
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       Feed my Friends is a tool on the internet websites of the Joint Research Centre (JRC) which
       enable individuals to exchange food via a digital community. Your personal data will be
       collected and further processed for the purposes detailed hereafter under point 2. For further
       details see DPO-2176.3 E-SERVICES ON JRC WEBSITES
       This app is operated by the Digital Earth and Reference Data Unit, Joint Research Centre
       (JRC). The Head of the Digital Earth Unit who manages the processing acts as personal data
       processor.
       As this processing collects and further processes personal data, Regulation (EC) 45/2001, of
       the European Parliament and of the Council of 18 December 2000 on the protection of
       individuals with regard to the processing of personal data by the Community institutions and
       bodies and on the free movement of such data, is applicable.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       2. What personal information do we collect, what is the legal basis,
       for what purpose and through which technical means?
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       Identification Data:
       The personal data collected and further processed by the app may include: email address,
       pictures taken with the app, address information entered into the app, location information
       used by the app, and the contents of conversations in the app.
       Legal Basis of processing:
       - The European Parliament and Council Decision of 18 Dec. 2006 on the EC 7th Research
       Framework Programme (FP7) (2007-2013).
       - Registration and participation of data subjects are provided on a purely voluntary basis.
       Purpose of processing:
       The purpose of the processing of personal data for the e-Services is to provide secure access
       and information exchange, management of data bases, and information for databases.
       Members may choose to turn off data analytics in the app preferences.
       Technical Information:
       The user data are collected via the app and are stored inside a Mongo database.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       3. Who has access to your information and to whom is it disclosed?
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       The possibility to update or erase the above personal data is only granted through Emailaddress /
       Password and to the system administrator of the software.
       The European Commission will not share data with third parties for direct marketing.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       4. How do we protect and safeguard your information?
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       The collected personal data is stored on the servers of JRC and underlie the Commission
       Decision C (2006) 3602 of 17/08/2006 “concerning the security of information systems used
       by the European Commission” defines IT security measures in force. Annex I defines the
       security requirements of EC Information Systems. Annex II defines the different actors and
       their responsibilities. Annex III defines the rules applicable by users. See notification
       DPO-1946.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       5. How can you verify, modify or delete your information?
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       Registered users have direct password-protected access to the app and are able to update it or
       to delete the app.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       6. How long do we keep your data?
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       Your personal data will remain in the database until the end of the project or for a maximum
       of 4 years starting from filling in the registration form on the app, unless the Controller
       launches an update of the registrations, asking the Data subjects to update or cancel the
       stored information including their personal data.
       Registered users have password protected direct access to their profile and can delete the app
       at any time. In some cases user_id's unused for a defined period are automatically deleted
       from the system.
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       7. Contact Information
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       Should you have any queries concerning the processing of your personal data, please address
       them to the controller.
       On questions relating to the protection of personal data, you can contact:
       - DG JRC Data Protection Co-ordinator: jrc-data-protection-coordinator@ec.europa.eu
       - Commission’s Data Protection Officer: data-protection-officer@ec.europa.eu
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       8. Recourse
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       In the event of a dispute, you can send a complaint to:
       • the European Data Protection Supervisor: edps@edps.europa.eu
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       COLLECTION OF ANONYMOUS TRAFFIC DATA IN THE APP
       </Text>
       </CardItem>
       <CardItem>
       <Text>
       When someone uses the Feed my Friends App anonymous site usage information is collected
       via various technical means. The usage of the site is recorded in log files. These files contain
       no personal information, nor information about which other sites the visitor has browsed.
       They allow the JRC to improve the service by knowing its usage, and are processed
       anonymously.
       </Text>
       </CardItem>
       <CardItem>
       <View style={styles.buttonscontainer}>
        <Button danger onPress={this.decline} style={styles.privacybuttons}>Decline Terms</Button>
        <Button primary onPress={this.accept} style={styles.privacybuttons}>Accept Terms</Button>
        </View>
       </CardItem>
       <CardItem>
       <Text style={styles.privacynotetext}>
       Note: You have to accept the privacy statement to use this app.
       </Text>
       </CardItem>
       </Card>
       </Content>
    </View>
     ) : null
   }

   {/* Signin Area starts here */}
   <Content theme={theme} style={styles.background}>
       <PushController
         onChangeToken={token => {
           this.setState({token: token || ""});
           if (Meteor.userId())
              Meteor.call('updateDeviceToken', Meteor.userId(), token);
         }
        }
       />

      <Image
      style={styles.logo}
      source={Logo}
      />
    <Text style={styles.subHeaderText}>Please Login</Text>
     <InputGroup>
     <Icon name='md-person' style={styles.listicon} />
     <Input
      placeholder='email address'
      onChangeText={(email) => this.updateState({ email })}
     />
     </InputGroup>
     <InputGroup>
     <Icon name='md-unlock' style={styles.listicon} />
     <Input
      placeholder='password'
      onChangeText={(password) => this.updateState({ password })}
      secureTextEntry={true}
     />
     </InputGroup>
     {this.state.confirmPasswordVisible ? (
     <Content>
     <InputGroup>
     <Icon name='md-unlock' style={styles.listicon}/>
     <Input
      placeholder='confirm password'
      onChangeText={(confirmPassword) => this.updateState({ confirmPassword })}
      secureTextEntry={true}
     />
     </InputGroup>
     </Content>
   ) : null}

    {this.state.error ? (
     <View style={styles.error}>
     <Text style={styles.errorText}>{this.state.error}</Text>
     </View>
      ) : (
      <View style={styles.noerror}>
      </View>
    )
    }

     {this.state.confirmPasswordVisible ? (
     <View style={styles.buttonscontainer}>
      <Button transparent onPress={this.signIn} style={styles.button}><Text style={styles.buttontext}>Login</Text></Button>
      <Button bordered info onPress={this.openLegal} style={styles.button}><Text style={styles.buttontext}>Create Account</Text></Button>
     </View>
     ) : (
     <View style={styles.buttonscontainer}>
     <Button transparent onPress={this.showConfirmPassword} style={styles.button}><Text style={styles.buttontext}>Create Account</Text></Button>
     <Button bordered info onPress={this.signIn} style={styles.button}><Text style={styles.buttontext}>Login</Text></Button>
     </View>
     )}
     </Content>
  </Container>
);
}
}

SignIn.propTypes = {
 updateState: React.PropTypes.func,
 signIn: React.PropTypes.func,
 createAccount: React.PropTypes.func,
 confirmPasswordVisible: React.PropTypes.bool,
 navigator: React.PropTypes.object,
};

export default SignIn;
