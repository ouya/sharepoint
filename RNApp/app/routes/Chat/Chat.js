import React, { Component } from 'react';
import { GiftedChat, Actions, Bubble, MessageText } from 'react-native-gifted-chat';

import { StyleSheet,  View , Dimensions, TouchableOpacity, Platform, TextInput} from 'react-native';
import Meteor, { createContainer, MeteorListView } from 'react-native-meteor';

import Loading from '../../components/Loading';
import { Button, Header, Container, Content, Tabs, List, ListItem, InputGroup, Input, Icon, Text } from 'native-base';
import theme from '../../themes/base-theme';
import Avatar from '../../components/Avatar';
import { COLORS } from '../../styles';

const styles = StyleSheet.create({
  chatcontainer:{
      flex: 0,
      height: Dimensions.get('window').height -130,
      width: Dimensions.get('window').width,
    },
    buttonoverlaycontainer:{
      position: 'absolute',
    },
  buttonoverlay: {
      flexDirection: 'row',
      marginLeft: Dimensions.get('window').width / 2 -104,
      marginTop: 0,
      marginBottom: 0,
      top: 0,
      zIndex: 100,
      flex: 0,
      justifyContent: 'center',
      backgroundColor:COLORS.transparent,
  },
  buttonsuccess: {
    marginTop: 10,
    zIndex: 100,
    alignSelf:'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
});

class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    };
    this.onSend = this.onSend.bind(this);
    this.setupChats = this.setupChats.bind(this);
    this.renderBubble = this.renderBubble.bind(this);
  }
  static route = {
    navigationBar: {
      title: 'Chat',
    }
  }
  // initial setup of all chats
  setupChats() {
    var messageDummyArray = [];
    for (var i = 0; i < this.props.fetchMessages.length; i++) {
      messageDummyArray.push(this.props.fetchMessages[i].chatData);
    }
    if (!messageDummyArray.length == 0) {
      messageDummyArray = messageDummyArray.reduce(function(a, b){
       return a.concat(b);
      });
    }

    this.setState({
      messages: messageDummyArray
    });
  }

  componentWillMount() {
    this.setupChats();
  }
  componentWillUnmount() {
    // handle.stop();
  }

  // update chats after onChange (meteor collection change)
  componentWillReceiveProps(nextProps) {
    var messageDummyArray = [];
    for (var i = 0; i < nextProps.fetchMessages.length; i++) {
      messageDummyArray.push(nextProps.fetchMessages[i].chatData);
    }
    if (!messageDummyArray.length == 0) {
      messageDummyArray = messageDummyArray.reduce(function(a, b){
       return a.concat(b);
      });
    }
    // console.log(messageDummyArray);
    this.setState({messages: messageDummyArray});
  }
  onSend(messages = []) {
    if (messages) { // if validation fails, value will be null
      // Meteor.collection('chats').insert({ chatData: messages, fooditem: this.props.fooditemId});
      // add chat message and notify users in the group
      console.log(this.props.fooditemId);
      Meteor.call('addChatMessage', messages, this.props.fooditemId, function(err, result) {
        if (err) {
          console.log(err);
        }
        else {
          console.log("succ addChatMessage");
        }
      });

      // also note down state in meteor.users  - which chat this user has writtin in
      Meteor.call('addActiveChat', this.props.fooditemId, Meteor.userId(), function(err, result) {
        if (err) {
          console.log(err);
        }
        else {
          console.log("succ addActiveChat");
        }
      });
    }

    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, messages),
      };
    });
  }
  exchangeComplete() {
    //can show rewards page maybe with gained points?
    this.setState({exchangeComplete: true});
  }

  handleSend(message = {}, rowID = null) {
   /*state or props is not working here (i want dynamically get the channelId), because this are refer to message, not parent class itself*/
  //  message.channelId = this.state.channelId;
  //  Meteor.call('Messages.insert', message);
  }
  renderBubble(props) {
      return (
        <Bubble
          {...props}
          wrapperStyle={{
            left: {
              backgroundColor: COLORS.inputBgBar,
            },
            right: {
              backgroundColor: COLORS.primary,
            }
          }}
        />
      );
    }
    renderMessageText(props) {
      const textStyle = {
        fontSize: 16, fontWeight: '400', color: '#ffffff',
      };
      const textStyleOther = {
        fontSize: 16, fontWeight: '400', color: '#000000',
      };
      const linkStyle = {fontSize: 16, color: '#ffffff'};
      return (
        <MessageText textStyle={{left: textStyleOther, right: textStyle}} linkStyle={{left: linkStyle, right: linkStyle}} {...props} />
      )
    }
  render() {
    if (!this.props.chatsReady) {
      return <Loading />;
    }
    else {
      return (
      <Container>
            <View style={styles.chatcontainer}>
            <GiftedChat
              handleSend={this.handleSend.bind(this)}
              renderAccessory={false}
              bottomOffset={270}
              renderBubble={this.renderBubble}
              renderMessageText={this.renderMessageText}
              renderFooter={this.renderFooter}
              messages={this.state.messages}
              onSend={this.onSend}
              user={{
                _id: Meteor.userId(),
                name: Meteor.user().profile.name,
              }}
            />
            </View>
      </Container>
      );
    }
  }
}

Chat.propTypes = {
  fetchMessages: React.PropTypes.array,
  chatsReady: React.PropTypes.bool
};

export default createContainer(({ fooditemId }) => {
  const handle = Meteor.subscribe('chats');
  return {
    // find all my chats that link with this fooditem (as well as implicitly to the provider then)
    fetchMessages: Meteor.collection('chats').find({fooditem: fooditemId}, {sort: { "chatData.createdAt" : -1 }}),
    chatsReady: handle.ready(),
  };
}, Chat);
