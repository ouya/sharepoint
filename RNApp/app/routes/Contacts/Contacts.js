import React, { Component } from 'react';

import { Header, Tabs, Button, Container, Content, Card, CardItem, Thumbnail, List, ListItem, InputGroup, Input, TextInput, Text, Badge } from 'native-base';
import theme from '../../themes/base-theme';
import Icon from 'react-native-vector-icons/Ionicons';

import { Dimensions, StyleSheet, View } from 'react-native';
import Meteor, { createContainer, MeteorListView, MeteorComplexListView } from 'react-native-meteor';
import Loading from '../../components/Loading';
import { COLORS } from '../../styles';
import Avatar from '../../components/Avatar';
import { Router } from '../../Router';

const styles = StyleSheet.create({
  maincontainer:{
    flex: 0,
    height:Dimensions.get('window').height -110,
    width: Dimensions.get('window').width,
  },
  buttonscontainer: {
    flex: 1,
    flexDirection: 'row',
  },
  buttonchat: {
    right: 10,
     marginLeft: 10,
     color: COLORS.primary,
     fontSize: 50,
     marginTop: 0,
    },
    buttonchattext: {
     marginTop: 12,
     marginLeft:10,
    },
    chatlistcontainer:{
      borderBottomColor: COLORS.transparent,
      height:Dimensions.get('window').height -155,

    },
  avatarlist:{
     width: 45,
   },
   searchicon:{
     color: COLORS.light,
     fontSize: 30,
     marginTop: 10,
     marginRight: 10,
   },
   searchinput:{
     color: COLORS.light,
     fontSize: 18,
     marginTop: 10,
     height: 30,
     padding: 0,
   },
   searchcontainer:{
     flex: 0,
     flexDirection: 'row',
     padding: 0,
     height: 46,
     backgroundColor: COLORS.inputBgBar,
   },
   friendsnametext:{
     marginTop: 3,
     fontSize: 16,

   },
});

class ChatsView extends React.Component {
    constructor(props) {
      super(props);
    }
    openChat(fooditemId) {
      this.props.navigator.push(Router.getRoute('chat',  {fooditemId: fooditemId}));
    }

    renderRow(activeChat) {
      if (activeChat) {
      return (
        <CardItem style={styles.buttonscontainer} button onPress={this.openChat.bind(this,activeChat._id)}>
              <Avatar username={activeChat.username} size={35}/>
              <Text style={styles.buttonchattext}>
              {activeChat.username} : {activeChat.itemName}
              </Text>
              <Icon style={styles.buttonchat} name="md-chatbubbles"/>
        </CardItem>
        );
      }
      else {
        return (null);
      }
      }
    render() {
      if (!(this.props.usersReady && this.props.foodReady)) {
        return (
          <Card style={styles.chatlistcontainer}>
          <Loading />
            <CardItem>
                  <Text style={styles.buttonchattext}>Sorry, you have no Chats by now.</Text>
            </CardItem>
          </Card>
        );
      }
      else {
        return (
          <Card style={styles.chatlistcontainer}>
            <MeteorComplexListView
              elements={()=>{
                var cursor = Meteor.collection('users').findOne( { _id: Meteor.userId() } );
                var activeChatNames = [];
                if (cursor) {
                  var activeChats = cursor.profile.activeChats;
                  for (var i = 0; i < activeChats.length; i++) {
                    var result = Meteor.collection('fooditems').findOne( { _id: activeChats[i] } );
                    activeChatNames.push(result);
                  }
                }
                return activeChatNames;
              }}
              renderRow={this.renderRow.bind(this)}
            />
            </Card>
          );
        }
      }
}

ChatsView.propTypes = {
  navigator: React.PropTypes.object,
  foodReady: React.PropTypes.bool,
  usersReady: React.PropTypes.bool
};


class FriendsView extends React.Component {
    constructor(props) {
      super(props);
      this.state = {searchActive: false};
    }
    openFriendProfile(userId) {
      this.props.navigator.push(Router.getRoute('profile', {userId : userId}));
    }

    renderRow(user) {
      // show name as prefix "maxMueller" for an address like maxMueller@gmail.com
      // var str = user.emails[0].address;
      // var displayName = str.substring(0,str.indexOf("@"));
      return (
        <CardItem iconRight style={styles.buttonscontainer} button onPress={this.openFriendProfile.bind(this,user._id)}>
        <Text style={styles.friendsnametext}>Name: {user.profile.name}</Text>
        <Badge primary>{user.profile.points} points</Badge>
        </CardItem>
      );
    }
    updateSearch(text) {
      if (text.length == 0) {
        this.setState({searchActive : false});
      }
      else {
        this.setState({searchText : text});
        this.setState({searchActive : true});
      }
    }

    render() {
      if (!this.props.usersReady) {
        return <Loading />;
      } else {
      return (
        <Card>
          <CardItem style={styles.searchcontainer} button>
          <Input style={styles.searchinput} onChangeText={(text) => this.updateSearch({text})} placeholder='Search your Friends'/>
          <Icon style={styles.searchicon} name="md-search" size={30} color={COLORS.primary}/>
          </CardItem>

          {this.state.searchActive ? (
            <MeteorComplexListView
            elements={()=>{
              return Meteor.collection('users').find({'profile.name' : {$regex: ".*" + this.state.searchText.text + ".*", $options: 'i'}});
            }}
            renderRow={this.renderRow.bind(this)}
            />
          ) : (
          <MeteorComplexListView
            elements={()=>{
              return Meteor.collection('users').find();
            }}
            renderRow={this.renderRow.bind(this)}
          />
        )
      }


        </Card>
        )
    }
  }
}

FriendsView.propTypes = {
  navigator: React.PropTypes.object,
  usersReady: React.PropTypes.bool
};


class Contacts extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    // handle.stop();
    // handle2.stop();
  }

  render() {
    return(
      <Container>
          <Content theme={theme}>
              <Tabs>
              <ChatsView navigator={this.props.navigator} usersReady={this.props.usersReady} foodReady={this.props.foodReady} tabLabel="Chats" />
              <FriendsView navigator={this.props.navigator} usersReady={this.props.usersReady} tabLabel="Friends - Members" />
              </Tabs>
          </Content>
      </Container>
  );
  }
}

Contacts.propTypes = {
  navigator: React.PropTypes.object,
  foodReady: React.PropTypes.bool,
  usersReady: React.PropTypes.bool
};

export default createContainer(() => {
  const handle = Meteor.subscribe('fooditems');
  const handle2 = Meteor.subscribe('users');
  return {
    foodReady: handle.ready(),
    usersReady: handle2.ready()
  };
}, Contacts);
