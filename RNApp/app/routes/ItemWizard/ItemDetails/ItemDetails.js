import React, { Component } from 'react';
import Mapbox, { MapView } from 'react-native-mapbox-gl';
import {
  AppRegistry,
  StyleSheet,
  Text,
  StatusBar,
  View,
  ScrollView,
  Alert
} from 'react-native';
import { Button, Container, Content, List, ListItem, InputGroup, Input } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

import theme from '../../../themes/base-theme';
import { COLORS } from '../../../styles';
import formstylesheet from '../../../styles/formstylesheet';
import { Router } from '../../../Router';

import Meteor from 'react-native-meteor';

var t = require('tcomb-form-native');

var Form = t.form.Form;

const styles = StyleSheet.create({
buttonscontainer: {
  flex: 1,
  flexDirection: 'row',
  marginTop: 10,
  alignItems: 'center',
  justifyContent:'center',
},
buttons: {
  paddingRight: 5,
  paddingLeft: 5,
},
addresslabel:{
  marginTop: 10,
  marginLeft: 10,
  fontSize: 17,
  color: COLORS.primary,
  fontWeight: '500',

},
addresstext:{
  marginTop: 2,
  marginBottom:10,
  marginLeft: 10,
  fontSize: 17,
  color: '#444444',
},
});

// here we are: define your domain model
var FoodItem = t.struct({
  itemName: t.String,              // a required string
  description: t.String,              // a required string
  // description: t.maybe(t.String),  // an optional string
  // location: t.String,               // a required number
  listDays: t.Number ,       // a boolean
});
var options = {
  stylesheet : formstylesheet,
  fields: {
    itemName: {
      placeholder: 'e.g. sweet bananas, box of grapes...'
    },
    description: {
      placeholder: 'e.g. whatever you think is important..'
    },
    listDays: {
      placeholder: 'available for how many days?'
    }
  }
}; // optional rendering options (see documentation)

class ItemDetails extends React.Component{

  constructor(props) {
    super(props);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit() {
    // call getValue() to get the values of the form
    var value = this.refs.form.getValue();
    console.log(this.props.imageId);
    if (value) { // if validation fails, value will be null
      Meteor.collection('fooditems').insert({
        itemName: value.itemName,
        description: value.description,
        listDays: value.listDays,
        address: this.props.myPlaces.properties.label,
        lng: this.props.myPlaces.geometry.coordinates[0],
        lat: this.props.myPlaces.geometry.coordinates[1],
        user: Meteor.userId(),
        username: Meteor.user().username,
        imageId: this.props.imageId,
        endDate: new Date(Date.now() + value.listDays * 24*60*60*1000)
      });
      // POINTS: add 2 points for publishing a new fooditemId
      Meteor.call('addPoints', Meteor.userId(), 2, function(err, result) {
        if (err) {
          console.log(err);
        }
        else {
          console.log("succ");
        }
      });

      Alert.alert(
            'Food published!',
            'Thank you for publishing your food!',
            [
              {text: 'See on Map', onPress: () => {
                // reset "add" stack to 0 - see we go back to itemcamera
                 var stackOfRoutes = this.props.navigator._getNavigatorState().routes;
                 var newRoutesStack = [stackOfRoutes[0]];
                 var itemLocation = { latitude: this.props.myPlaces.geometry.coordinates[1],
                                     longitude: this.props.myPlaces.geometry.coordinates[0] };
                 this.props.navigator.immediatelyResetStack(newRoutesStack, 0);
                 // reset "add" stack to 0 - see we go back to itemcamera
                 this.props.navigation.performAction(({ tabs, stacks }) => {
                   tabs('main').jumpToTab('map');
                   // this.props.navigator.push(Router.getRoute('map',  {location: this.state.location}));
                   stacks('map').push(Router.getRoute('map'));
                 });
                  // this.props.navigator.push(Router.getRoute('map', {location: itemLocation  }));
              }
              },
              {text: 'Publish more?', onPress: () =>
                {
                  // reset "add" stack to 0 - see we go back to itemcamera
                  var stackOfRoutes = this.props.navigator._getNavigatorState().routes;
                  var newRoutesStack = [stackOfRoutes[0]];
                  this.props.navigator.immediatelyResetStack(newRoutesStack, 0);
                }
              },

            ]
          )
    }
  }

  render() {
    return (
      <Container>
      <Content theme={theme}>
        <Text style={styles.addresslabel}>Item Location: </Text>
        <Text style={styles.addresstext}>{this.props.myPlaces.properties.label}</Text>
        <Form
            ref="form"
            type={FoodItem}
            options={options}
          />
        <View style={styles.buttonscontainer}>
        <Button success onPress={this.onFormSubmit} style={styles.buttons}> Publish Food Item ! </Button>
        </View>
        </Content>
        </Container>
            );

  }
};


ItemDetails.propTypes = {
  navigator: React.PropTypes.object,
  myPlaces: React.PropTypes.object,
  imageId: React.PropTypes.string,
};

export default ItemDetails;
