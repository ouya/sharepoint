import React from 'react';
import { Platform, StyleSheet, Text, View, TouchableHighlight, ListView, PixelRatio } from 'react-native';

import { Button, Container, Content, List, ListItem, InputGroup, Input } from 'native-base';
import theme from '../../../themes/base-theme';
import { COLORS } from '../../../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import Meteor from 'react-native-meteor';

import { Router } from '../../../Router';

var { MapzenPlacesAutocomplete } = require('react-native-mapzen-places-autocomplete');

const styles = StyleSheet.create({
buttonscontainer: {
  flex: 0,
  flexDirection: 'column',
  marginTop: 10,
  alignSelf: 'center',
  alignItems: 'center',
  justifyContent: 'center',
},
buttons: {
  paddingRight: 5,
  paddingLeft: 5,
},
});

class ItemLocation extends React.Component{
  constructor(props) {
    super(props);
    this.state = { hasLocation: false};
    this.onItemDetailsPress = this.onItemDetailsPress.bind(this);
  }

  // componentWillMount() {
  //   // be aware .. this is waiting for a callback to finish! so this.state.currentRegion is not
  //   // available until then
  //   if (navigator.geolocation) {
  //     navigator.geolocation.getCurrentPosition(
  //         (position) => {
  //         //  console.log(position);
  //           this.setState({currentRegion: {
  //             latitude: position.coords.latitude,
  //             longitude: position.coords.longitude,
  //           }});
  //           alert("current position is : LAT : " + position.coords.latitude + " / LNG : " + position.coords.longitude);
  //         },
  //         (error) => alert(error.message),
  //         {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000}
  //       );
  //   };
  // }

  onChange(value) {
    var fetchString = 'https://search.mapzen.com/v1/autocomplete?api_key=search-LVUGXaU&focus.point.lat=38.1&focus.point.lon=11.1&text=' + value.location;
    // console.log(fetchString);
    // retrieve nearby places from mapzen's search lib
    // var fetchString = 'https://search.mapzen.com/v1/autocomplete?api_key=search-LVUGXaU&focus.point.lat=48.1&focus.point.lon=11.4&text=Am%20Sulzbogen';
    fetch(fetchString)
      .then(function(response) {
        var places = response;
        // console.log(places);
        // this.setState({myPlaces: places});
        return places;
      }.bind(this))
      .catch(function(error) {
        console.log('There has been a problem with your mapzen fetch operation: ' + error.message);
      });

  }

  // onDetailsPress = (navigator) => {
  //
  // };
  onItemDetailsPress() {
    if (this.state.myPlaces && this.state.myPlaces.properties.label.length > 0) {
      this.props.navigator.push(Router.getRoute('itemdetails',  {myPlaces: this.state.myPlaces, imageId: this.props.imageId}));
    }
  }

  updateCurrentRegion(data, details) {
    this.setState({hasLocation: true});
    this.setState({myPlaces: data});
  }

  render() {
    // let locationLabel = "no places found";
    // if (this.state != null && this.state.myPlaces != null) locationLabel = this.state.myPlaces;
    // var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    // var dsSource = ds.cloneWithRows(['row 1', 'row 2','row 1', 'row 5']);
    return (
      <Container>
      <Content theme={theme}>
          {/* display */}
        <MapzenPlacesAutocomplete
                ref="autocomplete"
                placeholder='Where can your food be collected?'
                minLength={2} // minimum length of text to search
                autoFocus={true}
                fetchDetails={true}
                onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                  this.updateCurrentRegion(data, details);
                }}
                getDefaultValue={() => {
                  return ''; // text input default value
                }}
                query={{
                  // available options: https://developers.google.com/places/web-service/autocomplete
                  key: 'WM3QC2A',
                  language: 'en', // language of the results
                  types: '(cities)', // default: 'geocode'
                }}
                styles={{
                  description: {
                    fontWeight: 'bold',
                  },
                  predefinedPlacesDescription: {
                    color: COLORS.primary,
                  },
                  textInputContainer: {
                    backgroundColor: COLORS.inputBgBar,
                    height: 44,
                    borderTopColor: COLORS.tabBgColor,
                    borderBottomColor: COLORS.tabBgColor,
                    borderTopWidth: 0,
                    borderBottomWidth: 0,
                  },
                  textInput: {
                    backgroundColor: COLORS.inputBackground,
                    fontWeight: 'bold',
                    height: 28,
                    borderRadius: 10,
                    paddingTop: 4.5,
                    paddingBottom: 4.5,
                    paddingLeft: 10,
                    paddingRight: 10,
                    marginTop: 7.5,
                    marginLeft: 8,
                    marginRight: 8,
                    fontSize: 15,
                  },
                }}

                currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
                currentLocationLabel="Current location"
                nearbyPlacesAPI='MapzenPlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                GoogleReverseGeocodingQuery={{
                  // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                }}
                GooglePlacesSearchQuery={{
                  // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                  rankby: 'distance',
                  types: 'food',
                }}


                filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

                predefinedPlacesAlwaysVisible={false}
              />
            <View style={styles.buttonscontainer}>
              <Text></Text>
            {this.state.hasLocation
              ? <Button success onPress={this.onItemDetailsPress} style={styles.buttons}> Nearly done, go to last step! </Button>
            : <Button danger style={styles.buttons}>  No Location selected. </Button>}
            </View>
            </Content>
            </Container>
    );
  }
};

// MyPosts.propTypes = {
//   onDetailsPress: React.PropTypes.func,
// };
ItemLocation.propTypes = {
  navigator: React.PropTypes.object,
  imageId: React.PropTypes.string,
};

export default ItemLocation;
