import React, { Component } from 'react';
import {
  AppRegistry,
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';
import Camera from 'react-native-camera';
import { Button, Container, Content, List, ListItem, InputGroup, Input } from 'native-base';
import theme from '../../../themes/base-theme';
import { COLORS } from '../../../styles';
import Icon from 'react-native-vector-icons/Ionicons';

import Meteor from 'react-native-meteor';

import RNFS from 'react-native-fs';

import { Router } from '../../../Router';

const styles = StyleSheet.create({
  preview: {
    flex: 0,
    zIndex: 20,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height -110,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonscontainer: {
    flexDirection: 'row',
    marginBottom: 10,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  buttoncam: {
    marginLeft: Dimensions.get('window').width / 2 - 30,
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    alignItems:'center',
    justifyContent:'center',
    paddingBottom: 3,
  },
  buttonicon:{
    backgroundColor: 'rgba(0,0,0,0)',
    color: COLORS.buttonIconColor,
  },
  buttonwarn: {
    marginLeft: Dimensions.get('window').width / 4,
    marginTop: 20,
    right:5,
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
    alignItems:'center',
    justifyContent:'center',
    paddingBottom: 0,
  },
});

class ItemCamera extends React.Component {
  constructor(props) {
    super(props);
  }

  /*
    data: Returns a base64-encoded string with the capture data (only returned with the deprecated Camera.constants.CaptureTarget.memory)
    path: Returns the path of the captured image or video file on disk
  */
  takePicture() {
      var self = this;
      this.camera.capture()
        .then( ( data ) => {
            RNFS.readFile( data.path.replace('file:/', ''), 'base64' )
              .then( res => {
                                Meteor.call('imageUpload', res, function(err, result) {
                                  if (err) {
                                    console.log(err);
                                  }
                                  else {
                                    self.nextLocation(result);
                                  }
                                });
                            }
                    )

        })
        .catch(err => console.error(err));
  }

  nextLocation(result) {
    this.props.navigator.push(Router.getRoute('itemlocation',  { imageId: result }));
  }

  noPic() {
    this.nextLocation(""); // "" no image id
  }

  render() {
    if (Platform.OS === 'android' || Platform.OS === 'ios') {
      return (

        <Content theme={theme}>
            <Camera
              ref={(cam) => {
                this.camera = cam;
              }}
              style={styles.preview}
              aspect={Camera.constants.Aspect.fill}
              captureAudio={false}
              captureTarget={Camera.constants.CaptureTarget.disk}
              captureQuality={Camera.constants.CaptureQuality.medium}
              type={Camera.constants.Type.back}
            >
            <View style={styles.buttonscontainer}>
            <Button sucess style={styles.buttoncam} onPress={this.takePicture.bind(this)}>
            <Icon name="md-camera" size={45} style={styles.buttonicon}/>
            </Button>
            <Button warning style={styles.buttonwarn} onPress={this.noPic.bind(this)}>
            <Icon name="md-close-circle" size={32} style={styles.buttonicon}/>
            </Button>
            </View>
          </Camera>
          </Content>

      );
    }
    else {
      return (
        <View>
          <Text style={styles.main}>
            Camera item - you are using a browser - no camera available
          </Text>
        </View>
      );
    }
  }
}

ItemCamera.propTypes = {
  navigator: React.PropTypes.object,
};

export default ItemCamera;
