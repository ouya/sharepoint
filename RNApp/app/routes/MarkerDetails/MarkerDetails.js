import React, { PropTypes } from 'react';
import {
  Alert,
  AppRegistry,
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
  View,
  ScrollView
} from 'react-native';
import Loading from '../../components/Loading';
import { Button, Container, Content, Card, CardItem, Thumbnail, List, ListItem, InputGroup, Input, Text} from 'native-base';
import theme from '../../themes/base-theme';
import { COLORS } from '../../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { Router } from '../../Router';

import Meteor, { createContainer } from 'react-native-meteor';

var moment = require('moment');

const styles = StyleSheet.create({
  noimagecontainer:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
    height: Dimensions.get('window').height -296,
    width: Dimensions.get('window').width,
    },
    noimageicon:{
      alignSelf: 'center',
      fontSize: 55,
    },
    noimagetext:{
      flex:0,
      marginTop: 3,
      marginBottom: 8,
      alignSelf: 'center',
      fontSize: 13,
    },
    itemcontainer:{
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-start',
    },
    textcontainer:{
      flexDirection: 'column',
      justifyContent: 'flex-start',
    },
    thumbnailimage:{
      marginLeft: -10,
      marginBottom: -11,
      marginTop: -7,
      height: 60,
      width: 60,
    },
  imagecontainer:{
    resizeMode: 'cover',
    height: Dimensions.get('window').height -296,
    width: Dimensions.get('window').width / 5 * 3,
  },
  editbuttonscontainer:{
    marginTop: 5,
    flex:0,
    marginLeft: 10,
    alignSelf: 'flex-end',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  editbutton:{
    height: 50,
    width: 50,
    backgroundColor: COLORS.primary,
    marginRight: 5,
  },
  deletebutton:{
    height: 50,
    width: 50,
    backgroundColor: COLORS.danger,
  },
  editicon:{
    color: COLORS.sidebar,
    fontSize: 45,
  },
  deleteicon:{
    color: COLORS.sidebar,
    fontSize: 45,
  },
  titletext:{
    flex: 0,
    top: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'flex-start',
    fontSize: 16,
    fontWeight: 'bold',
  },
  addresscontainer:{
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: COLORS.primary,
  },
  addresstitle:{
    marginTop: -10,
    fontSize: 15,
    width: 70,
    color: COLORS.sidebar,
    fontWeight: 'bold',
  },
  addresstext:{
    marginTop: -10,
    marginBottom: -10,
    fontSize: 15,
    width: Dimensions.get('window').width -120,
    color: COLORS.sidebar,
    fontWeight: 'bold',
  },
  addressmapicon:{
  marginTop: -3,
  marginBottom: -10,
  fontSize: 30,
  color: COLORS.sidebar,
  },
  descriptiontext:{
     padding: 10,
   },
  datetext:{
    color: '#888888',
  },
  buttonscontainer: {
    borderWidth: 1,
    backgroundColor: COLORS.primary,
    height: 50,
    padding: 10,
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttons: {
    paddingRight: 5,
    paddingLeft: 5,
  },
  buttonchaticon: {
    color: COLORS.info,
    fontSize: 24,
    marginRight: 5,
    marginTop: 2,
  },
  buttonchattext: {
    marginTop: 0,
    fontSize: 18,
    fontWeight: "600",
    color: COLORS.info,
  },
});

  // var PushNotification = require('react-native-push-notification');

class MarkerDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = { hasImage: false, detailImage: null};
    this.sendMessage = this.sendMessage.bind(this);
    this.openMap = this.openMap.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    var foodImage;
    if (nextProps.fooditem[0])
      foodImage = Meteor.collection('foodimages').findOne({_id: nextProps.fooditem[0].imageId});
    if (nextProps.fooditem[0]) {
      this.setState({
        _id: nextProps.fooditem[0]._id,
        itemName: nextProps.fooditem[0].itemName,
        address: nextProps.fooditem[0].address,
        description: nextProps.fooditem[0].description,
        listDays: nextProps.fooditem[0].listDays,
        endDate: moment(nextProps.fooditem[0].endDate).format("MMM Do YY"),
        provider: nextProps.fooditem[0].user,
        username: nextProps.fooditem[0].username,
        location: { latitude: nextProps.fooditem[0].lat,
                    longitude: nextProps.fooditem[0].lng },
      });
    }
    if (foodImage) {
      this.setState({
        hasImage: true,
        detailImage: 'data:image/png;base64,' + foodImage.image
      });
    }
  }
  sendMessage() {
    this.props.navigator.push(Router.getRoute('chat',  {fooditemId: this.props.fooditemId}));
  }
  openMap() {
    this.props.navigator.push(Router.getRoute('map',  {location: this.state.location}));
  }
  onExchangeItem() {
    var self = this;
    // check whether you can exchange - only allowed if you have chatted with sb
    Meteor.call('foodItemHasChats', this.props.fooditemId, function(err, result) {
      if (err) {
        console.log(err);
      }
      else {
       if (result)// DO EXCHANGE Alert
        Alert.alert(
          'Exchanged item',
          'Did you exchange this item with a member that contacted you?',
          [
            {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
            {text: 'Yes', onPress: () =>
              {
                // if this fooditem has chats -> then
                // 1. give 10 points for provider
                Meteor.call('addPoints', Meteor.userId(), 10, function(err, result) {
                  if (err) {
                    console.log(err);
                  }
                  else {
                    console.log("succ points for provider");
                  }
                });

                // 2. give 1 point for each chat person
                Meteor.call('addPointsForChatPersons', self.props.fooditemId, 1, function(err, result) {
                  if (err) {
                    console.log("addPointsForChatPersons");
                    console.log(err);
                  }
                  else {
                    console.log("succ points for chat persons");
                    Meteor.call('deleteFooditem', self.state._id, function(err, result) {
                      if (err) {
                        console.log(err);
                      }
                      else {
                        Alert.alert(
                          'Fooditem exchanged and removed!',
                          'You receive 10 points!',
                          [
                            {text: 'OK', onPress: () => self.props.navigator.push(Router.getRoute('profile'))},
                          ]
                        );
                      }
                    });
                  }
                });
              }
            },
          ]
        );
        else {
          // DO NO-EXCHANGE allowed Alert
          Alert.alert(
            'Fooditem exchange not allowed',
            'Wait until a friend contacts you!',
            [
              {text: 'OK'},
            ]
          );
        }
      }
    });
  }

  componentDidMount() {
  }

  componentWillMount() {
  }

  onDeleteItem() {
    //
    // FCM.presentLocalNotification({
    //             // id: "UNIQ_ID_STRING",                               // (optional for instant notification)
    //             title: "My Notification Title",                     // as FCM payload
    //             body: "My Notification Message",                    // as FCM payload (required)
    //             sound: "default",                                   // as FCM payload
    //             priority: "high",                                   // as FCM payload
    //             click_action: "ACTION",                             // as FCM payload
    //             badge: 10,                                          // as FCM payload IOS only, set 0 to clear badges
    //             number: 10,                                         // Android only
    //             ticker: "My Notification Ticker",                   // Android only
    //             auto_cancel: true,                                  // Android only (default true)
    //             large_icon: "ic_launcher",                           // Android only
    //             icon: "ic_notification",                            // as FCM payload
    //             big_text: "Show when notification is expanded",     // Android only
    //             sub_text: "This is a subText",                      // Android only
    //             color: "red",                                       // Android only
    //             vibrate: 300,                                       // Android only default: 300, no vibration if you pass null
    //             tag: 'some_tag',                                    // Android only
    //             group: "group",                                     // Android only
    //             my_custom_data:'my_custom_field_value',             // extra data you want to throw
    //             lights: true,                                       // Android only, LED blinking (default false)
    //             show_in_foreground: true                                  // notification when app is in foreground (local & remote)
    //         });

    Alert.alert(
      'Delete item',
      'Do you want to delete this item ?',
      [
        {text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel'},
        {text: 'Yes', onPress: () =>
          {
            var self = this;
            Meteor.call('deleteFooditem', this.state._id, function(err, result) {
              if (err) {
                console.log(err);
              }
              else {
                self.props.navigator.pop();
              }
            });
          }
        },
      ]
    );
  }

  componentWillUnmount() {
    // this.props.handle.stop();
  }

  renderEmptyButtons() {
    return  (
      <View style={styles.editbuttonscontainer}>
      </View>
    );
  }

  renderEditButtons() {
    return  (
      <View style={styles.editbuttonscontainer}>
      <Button style={styles.editbutton} onPress={this.onExchangeItem.bind(this)}>
      <Icon style={styles.editicon} name="md-checkmark-circle"/>
      </Button>
      <Button style={styles.deletebutton} onPress={this.onDeleteItem.bind(this)}>
      <Icon style={styles.deleteicon} name="md-trash"/>
      </Button>
      </View>
    );
  }
  render() {
      return (
        <Container>
        <Content theme={theme}>
        <Card>
          <CardItem >
            {!this.state.hasImage ? (
              <Thumbnail source={require('../../images/Logo.png')} />
              ) : (
                <Thumbnail style={styles.thumbnailimage} source={{uri: this.state.detailImage}} />
              )}
                <View style={styles.textcontainer}>
                  <Text style={styles.titletext}>{this.state.itemName}</Text>
                  <Text style={styles.datetext}>Valid till: {this.state.endDate}</Text>
                  </View>
                  {(this.state.provider === Meteor.userId()) ? (
                    this.renderEditButtons()
                  ) : this.renderEmptyButtons() }
          </CardItem>
          <CardItem button onPress={this.sendMessage} style={styles.buttonscontainer}>
                <Text style={styles.buttonchattext}>chat with {this.state.username}</Text>
                <Icon style={styles.buttonchaticon} name="md-chatbubbles" size={45} color={COLORS.primary}/>
            </CardItem>

            {!this.state.hasImage ? (
              <CardItem style={styles.noimagecontainer}>
                  <Icon name="md-warning" style={styles.noimageicon}/>
                  <Text style={styles.noimagetext}>Sorry - no image provided</Text>
                  <Content style={styles.descriptiontext}>
                  <Text>
                    Description:
                    </Text>
                  <Text>
                   {this.state.description}
                    </Text>
                    </Content>
               </CardItem>
                    ) : (
                  <CardItem style={styles.itemcontainer}>

                  <Image style={styles.imagecontainer} source={{uri: this.state.detailImage}}/>
                  <Content style={styles.descriptiontext}>
                  <Text>
                    Description:
                    </Text>
                  <Text>
                   {this.state.description}
                    </Text>
                    </Content>
               </CardItem>)
              }
              <CardItem button onPress={this.openMap} style={styles.addresscontainer} >
              <Text style={styles.addresstitle}>
                 Address:
                </Text>
              <Text style={styles.addresstext}>
                 {this.state.address}
                </Text>
                  <Icon name="md-map" style={styles.addressmapicon}/>
                </CardItem>

          </Card>
          </Content>
          </Container>
        );
  }
}


MarkerDetails.propTypes = {
  navigator: React.PropTypes.object,
  handle: PropTypes.bool,
};

export default createContainer(({fooditemId}) => {
  const handle = Meteor.subscribe('foodWithImage', fooditemId);
  return {
    handle: handle.ready(),
    fooditem: Meteor.collection('fooditems').find( { _id: fooditemId } )
  };
}, MarkerDetails);
