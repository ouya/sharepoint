import React, { Component } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Router } from '../../Router';

import { StyleSheet, Text, TextInput, View, Image, Dimensions } from 'react-native';
// import Button from '../../components/Button';
import { Button, Badge, Header, Container, Card, CardItem, Content, Tabs, List, ListItem, InputGroup, Input, Icon } from 'native-base';
import theme from '../../themes/base-theme';

import Avatar from '../../components/Avatar';
import { COLORS } from '../../styles';

import Fig from '../../images/rewards/Fig.png';
import Apple from '../../images/rewards/Apple.png';
import Tomato from '../../images/rewards/Tomato.png';
import Pomegranates from '../../images/rewards/Pomegranates.png';
import Raspberries from '../../images/rewards/Raspberries.png';
import Eggplant from '../../images/rewards/Eggplant.png';
import Pineapples from '../../images/rewards/Pineapples.png';
import Bananas from '../../images/rewards/Bananas.png';
import Strawberry from '../../images/rewards/Strawberry.png';
import Carrot from '../../images/rewards/Carrot.png';

const window = Dimensions.get('window');
const styles = StyleSheet.create({
  cardcontainer:{
    height: window.height -110,
  },
  rewardimage:{
    height: 300,
    width: 300,
    alignSelf: 'center',
    resizeMode: 'cover',
    marginTop: -30,
  },
  avatarstyles:{
    alignSelf: 'center',
    marginTop: -55,
  },
  buttons: {
    marginRight: 10,
    paddingRight: 5,
    paddingLeft: 5,
  },
  buttonscontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 0,
  },
  usernamecontainer: {
    flexDirection: 'row',
    marginTop: -5,
    marginBottom: -10,
    justifyContent: 'center'
  },
  changeusernamecontainer: {
    flexDirection: 'row',
    marginTop: 0,
    marginBottom: 0,
    justifyContent: 'center'
  },
  emailtext: {
    marginTop: 17,
    marginRight:10,
    fontSize: 18,
  },
  inputusername:{
  borderColor: COLORS.inputBgBar,
  backgroundColor: COLORS.inputBackground,
  color: COLORS.primary,
  width: 200,
  fontSize: 18,
  height: 35,
  marginTop: 12,
  marginRight: 10,
  padding: 0,
  },
  inputplaceholder:{
    color: COLORS.primary,
    fontSize: 18,
  },
  usernamecancel:{
    fontSize: 40,
    marginRight: 2,
    marginTop: 0,
    color: COLORS.danger,
  },
  friendnametext:{
    marginTop: 5,
    fontSize: 18,
    },
    pointbadge:{
      alignSelf: 'center',
      marginTop: 5,
      marginRight: 5,
      marginBottom: -15,
      zIndex: 5,
    }
});
class Profile extends Component {
  constructor(props) {
    super(props);
    this.signOut = this.signOut.bind(this);
    this.toggleState = this.toggleState.bind(this);
    this.showPosts = this.showPosts.bind(this);
    this.showRewards = this.showRewards.bind(this);
    this.cancel = this.cancel.bind(this);
    this.getUserName = this.getUserName.bind(this);
    this.state = {
      newusername: "",
      setusername: false
    };
  }

  componentWillReceiveProps(nextProps) {
    }

  signOut() {
    var self = this;
    Meteor.logout(function(err) {
      self.props.navigator.push(Router.getRoute('signin'));
      Meteor.logout();
    });
  }

  showPosts() {
    this.props.navigator.push(Router.getRoute('myposts', {owner: this.props.friend}));
  }

  showRewards() {
    console.log
    this.props.navigator.push(Router.getRoute('showreward', {friend: this.props.friend}));
  }

  toggleState() {
    if (this.state.setusername) {
      if (this.state.newusername.length > 0) {
        Meteor.collection('users').update(Meteor.userId(), {
                    $set: {
                        'profile.name': this.state.newusername,
                    }
        });
      }
      else return;
    }
    this.setState({ setusername: !this.state.setusername });
  }

  cancel() {
      this.setState({ setusername: false });
  }
  getUserName() {
    if (Meteor.user())
      if (typeof Meteor.user().profile.name == "undefined") {
        return Meteor.user().username;
      }
      else {
        return Meteor.user().profile.name;
      }
    else {
      return "no user";
    }
  }


  renderReward() {
    var rewardlevel;
    const {friend} = this.props;
    if (!this.props.ownprofile) {
      rewardlevel = friend[0].profile.rewardlevel;
    }
    else {
      rewardlevel = Meteor.user().profile.rewardlevel;
    }

    switch (rewardlevel) {
      case 0: return (<Image style={styles.rewardimage} source={Fig} />);break;
      case 1: return (<Image style={styles.rewardimage} source={Apple} />);break;
      case 2: return (<Image style={styles.rewardimage} source={Tomato} />);break;
      case 3: return (<Image style={styles.rewardimage} source={Pomegranates} />);break;
      case 4: return (<Image style={styles.rewardimage} source={Raspberries} />);break;
      case 5: return (<Image style={styles.rewardimage} source={Eggplant} />);break;
      case 6: return (<Image style={styles.rewardimage} source={Carrot} />);break;
      case 7: return (<Image style={styles.rewardimage} source={Pineapples} />);break;
      case 8: return (<Image style={styles.rewardimage} source={Bananas} />);break;
      case 9: return (<Image style={styles.rewardimage} source={Strawberry} />);break;

      default:
    }
  }

  render() {
      const {friend} = this.props;
      if (!this.props.ownprofile) {
        return (
          <Container>
          <Content theme={theme}>
          <Card style={styles.cardcontainer}>
          {/* Delete the next row after finishing tests */}
          <Badge primary style={styles.pointbadge}>Your friend already has {friend[0].profile.points} points. Awesome.</Badge>
          <CardItem>
            {this.renderReward()}
          </CardItem>
          <View style={styles.avatarstyles}>
          <Avatar username={friend[0].profile.name} size={60}/>
          </View>
          <CardItem>
          <View style={styles.usernamecontainer}>
            <Text style={styles.friendnametext}>User name: {friend[0].profile.name}</Text>
          </View>
          </CardItem>
      <CardItem>
      {/* remove this buttons maybe? thought would be nice to see all active items of the user. */}
      <View style={styles.buttonscontainer}>
      <Button primary onPress={this.showPosts} style={styles.buttons}>{friend[0].profile.name}'s Posts </Button>
      </View>
      </CardItem>
      </Card>
      </Content>
  </Container>
      )
      } else {
        if (Meteor.user())
        // here comes the own profile
        return (
            <Container>
            <Content theme={theme}>
            <Card style={styles.cardcontainer}>
            <Badge primary style={styles.pointbadge}>You have {Meteor.user().profile.points} points.</Badge>
            <CardItem>
              {this.renderReward()}
            </CardItem>
            <View style={styles.avatarstyles}>
            <Avatar username={this.getUserName()} size={60}/>
            </View>
            <CardItem>
            <View style={styles.usernamecontainer}>
            {this.state.setusername ? (
              <View style={styles.changeusernamecontainer}>
              <Input style={styles.inputusername} placeholder='Please add a name.' placeholderStyle={styles.inputplaceholder} onChangeText={(newusername) => this.setState({newusername})} />
              <Button large transparent danger onPress={this.cancel} style={styles.usernamecancel}>
              <Icon name='md-close-circle' style={styles.usernamecancel}/>
              </Button>
              </View>
            ) : (
              <Text style={styles.emailtext}>Name: {this.getUserName()}</Text>
            )
          }

          {this.state.setusername ? (
            <Button large transparent onPress={this.toggleState} style={styles.usernamebuttons}>
            <Icon name='md-checkmark-circle' style={styles.usernamebuttons}/>
            </Button>
          ) : (
            <Button large transparent onPress={this.toggleState} style={styles.usernamebuttons}>
            <Icon name='md-settings' style={styles.usernamebuttons}/>
            </Button>
          )
        }
        </View>
        </CardItem>
        <CardItem>
        <View style={styles.buttonscontainer}>
        <Button primary onPress={this.showPosts} style={styles.buttons}> My Posts </Button>
        <Button success onPress={this.showRewards} style={styles.buttons}> Last Reward </Button>
        <Button danger onPress={this.signOut} style={styles.buttons}> Sign out </Button>
        </View>
        </CardItem>
        </Card>
        </Content>
    </Container>
        );
        else return (null);
  }
}
}
Profile.propTypes = {
  user: React.PropTypes.object,
  navigator: React.PropTypes.object,
  friend: React.PropTypes.array,
  handle: React.PropTypes.bool,
  ownprofile: React.PropTypes.bool
};

export default createContainer(({userId}) => {
  var ownprofile = false;
  if (userId === undefined){
  ownprofile = true;
  }
  if (userId === Meteor.userId()){
  ownprofile = true;
  }
  const handle = Meteor.subscribe('users');
  return {
    friend: Meteor.collection('users').find( { _id : userId } ),
    user: Meteor.user(),
    ownprofile: ownprofile,
  };
}, Profile);
