import React, { PropTypes } from 'react';
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  StatusBar,
  View,
  ScrollView,
  Dimensions
} from 'react-native';
import { MeteorListView, MeteorComplexListView } from 'react-native-meteor';
import Loading from '../../../components/Loading';
import { COLORS } from '../../../styles';
import theme from '../../../themes/base-theme';
import { Button, Header, Card, CardItem, Thumbnail, Container, Content, Tabs, List, ListItem, InputGroup, Input, Icon } from 'native-base';
import { Router } from '../../../Router';

import Meteor, { createContainer } from 'react-native-meteor';

const styles = StyleSheet.create({
  itemcontainer: {
    flex: 0,
    flexDirection: 'row',
  },
  noimageicon:{
    alignSelf: 'flex-start',
    fontSize: 55,
    height: 80,
    marginLeft: 15,
    marginRight: 12,
  },
  imageicon:{
    height: 80,
    width: 80,
  },
  textcontainer:{
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  titletext:{
    flex: 0,
    top: 0,
    marginTop: 0,
    marginBottom: 0,
    alignSelf: 'flex-start',
    fontSize: 16,
    fontWeight: 'bold',
  },
  descriptiontext:{
    flex:0,
    marginTop: 2,
    marginBottom: 0,
    alignSelf: 'flex-end',
    fontSize: 13,
    width: Dimensions.get('window').width -100,
  },
});

class MyPosts extends React.Component {

  constructor(props) {
    super(props);
  }

  showItem(fooditemId) {
    this.props.navigator.push(Router.getRoute('markerdetails', {fooditemId : fooditemId}));
  }

  componentWillUnMount() {
    // handle.stop();
  }

  renderRow(fooditem) {
    if (fooditem.foodimage) {
      var foodItemURI = 'data:image/png;base64,' + fooditem.foodimage.image;
      return (
      <Card>
       <CardItem style={styles.buttonscontainer} button onPress={this.showItem.bind(this,fooditem._id)}>
        <Thumbnail source={{uri: foodItemURI}} style={styles.imageicon} />
        <View style={styles.textcontainer}>
         <Text style={styles.titletext}>{fooditem.itemName}</Text>
         <Text numberOfLines={4} ellipsizeMode="tail" style={styles.descriptiontext}>{fooditem.description}</Text>
         </View>
       </CardItem>
      </Card>
     );
    }
    else {
      return (
      <Card>
       <CardItem style={styles.itemcontainer} button onPress={this.showItem.bind(this,fooditem._id)}>
           <Icon name="md-warning" style={styles.noimageicon}/>
           <View style={styles.textcontainer}>
              <Text style={styles.titletext}>{fooditem.itemName}</Text>
              <Text numberOfLines={4} ellipsizeMode="tail" style={styles.descriptiontext}>{fooditem.description}</Text>
           </View>
       </CardItem>
       </Card>
     );
    }

  }

  render() {
    if (!this.props.handle) {
      return(
        <Card>
        <CardItem>
              <Text style={styles.buttonchattext}>Your Items are being loaded ...</Text>
        </CardItem>
        </Card>
      );
    }
    else

    return(
      <Container>
      <Content theme={theme}>
      <MeteorComplexListView
      elements={()=>{
        return Meteor.collection('fooditems').find({user : this.props.userId}).map((fooditem) => {
          const foodimage = Meteor.collection('foodimages').findOne({_id: fooditem.imageId});
          return {
            ...fooditem,
            foodimage
          };
        })
      }}
      renderRow={this.renderRow.bind(this)}
      />
        </Content>
      </Container>
  );

  }
}


MyPosts.propTypes = {
  navigator: React.PropTypes.object,
  handle: React.PropTypes.any.isRequired,
  owner: React.PropTypes.object,
};

export default createContainer(({owner}) => {
  var userId;
  if (owner.length == 0) {
    userId = Meteor.userId();
  }
  else {
    userId = owner[0]._id;
  }

  const handle = Meteor.subscribe('myposts', userId);
  return {
    handle: handle.ready(),
    userId: userId
  };
}, MyPosts);
