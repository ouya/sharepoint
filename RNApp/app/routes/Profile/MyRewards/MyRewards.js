import React, { PropTypes } from 'react';
import {
  AppRegistry,
  Image,
  StyleSheet,
  StatusBar,
  View,
  ScrollView
} from 'react-native';
import { MeteorListView } from 'react-native-meteor';
import Loading from '../../../components/Loading';
import { Container, Content, CardSwiper, DeckSwiper, Picker, Card, CardItem, Text,  Thumbnail } from 'native-base';
import theme from '../../../themes/base-theme';
import { COLORS } from '../../../styles';
import Icon from 'react-native-vector-icons/Ionicons';

import { Router } from '../../../Router';

import Meteor, { createContainer } from 'react-native-meteor';

const styles = StyleSheet.create({
});

const Item = Picker.Item;

let rewardslist = [
  {
    name: 'The British word for eggplant, aubergine, comes from Sanskrit, the ancient language of India. It may be that the plant originated there.',
    title: 'Eggplant',
    image: require('../../../images/rewards/Eggplant.png')
}, {
    name: 'Tomatoes, eggplants, peppers, and potatoes are members of the poisonous nightshade family. Don’ t worry these vegetables and fruits are safe to eat, but don’ t eat the leaves or other parts!',
    title: 'Tomato',
    image: require('../../../images/rewards/Tomato.png')
}, {
    name: 'Carrots used to be purple before the 17th century. Orange carrots were less common until horticulturists bred them to be orange and sweet.',
    title: 'Carrot',
    image: require('../../../images/rewards/Carrot.png')
}, {
    name: 'There are over 7500 varieties of apples in the world. How many have you tasted?',
    title: 'Apple',
    image: require('../../../images/rewards/Apple.png')
}, {
    name: 'Strawberries can grow from seeds, but most new plants grow from runners - a stem that grows its own roots.How versatile!',
    title: 'Strawberry',
    image: require('../../../images/rewards/Strawberry.png')
}, {
    name: 'Industrial quality paper and clothing can be made from banana plants. Banana plants grow very quickly - unlike the trees traditionally used to make paper.',
    title: 'Bananas',
    image: require('../../../images/rewards/Bananas.png')
}, {
    name: 'Pineapples are grown by planting the tops of old pineapple fruits into the ground. A new pineapple bush grows up from the leaves.',
    title: 'Pineapples',
    image: require('../../../images/rewards/Pineapples.png')
}, {
    name: 'Raspberries come in all kinds of colors: yellow, black, purple, and red.',
    title: 'Raspberries',
    image: require('../../../images/rewards/Raspberries.png')
}, {
    name: 'Pomegranates can be stored up to two months in the refrigerator.',
    title: 'Pomegranates',
    image: require('../../../images/rewards/Pomegranates.png')
}, {
    name: 'All the flowers of the fig are on the inside of the fruit. The only way for the fig fruit to be fertilized is through the aid of a special wasp.These wasps are specially suited to the fig that they pollinate.',
    title: 'Fig and Wasp',
    image: require('../../../images/rewards/Fig.png')
}];

class MyRewards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        eggplant : [],
        tomato : [],
        carrot : [],
        apple : [],
        strawberry : [],
        bananas : [],
        pineapples : [],
        raspberries : [],
        pomegranates : [],
        fig : []
    }
}

componentDidMount() {
    this.setState({
        eggplant : rewardslist[0],
        tomato : rewardslist[1],
        carrot : rewardslist[2],
        apple : rewardslist[3],
        strawberry : rewardslist[4],
        bananas : rewardslist[5],
        pineapples : rewardslist[6],
        raspberries : rewardslist[7],
        pomegranates : rewardslist[8],
        fig : rewardslist[9]
    })
}
next() {
    let currentReward = rewardslist.indexOf(this.state.item);
    let newReward = currentReward + 1;

    this.setState({
        item: rewardslist[newReward > rewardslist.length - 1 ? 0 : newReward],
        item2: rewardslist[newReward > rewardslist.length - 1 ? 1 : newReward-1]
    });
}
previous() {
    let currentReward = rewardslist.indexOf(this.state.item);
    let newReward = currentReward - 1;

    this.setState({
        item: rewardslist[newReward < rewardslist.length ? rewardslist.length-1 : newReward],
        item2: rewardslist[newReward < rewardslist.length ? rewardslist.length-2 : newReward-2]
    });
}
  render() {
      return (
        <Container>
                <Content theme={theme}>
                    <DeckSwiper
                        onSwipeRight={()=>this.next()}
                        onSwipeLeft={()=>this.previous()}
                        dataSource={rewardslist}
                        renderItem={(item)=>
                            <Card style={{elevation: 3}}>
                                <CardItem>
                                    <Thumbnail source={require('../../../images/rewards/Apple.png')} />
                                    <Text>{item.title}</Text>
                                    <Text note>1 of 5 rewards collected!</Text>
                                </CardItem>
                                <CardItem>
                                    <Image style={{ resizeMode: 'repeat', width: null }} source={item.image} />
                                </CardItem>
                                <CardItem>
                                    <Text>{item.name}</Text>
                                </CardItem>
                            </Card>
                        }>
                    </DeckSwiper>
                </Content>
            </Container>
      );
  }
}


MyRewards.propTypes = {
  navigator: React.PropTypes.object,
  handle: PropTypes.bool,
};

export default createContainer(({chatGroupId}) => {
  const handle = Meteor.subscribe('foodWithImage', chatGroupId);
  // const itemHandle = Meteor.subscribe('fooditems', chatGroupId);
  return {
    handle: handle.ready(),
    foodimage: Meteor.collection('foodimages').find(),
    fooditem: Meteor.collection('fooditems').find()
  };
}, MyRewards);
