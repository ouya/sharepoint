import React, { Component } from 'react';
import Mapbox, { MapView } from 'react-native-mapbox-gl';
import {
  AppRegistry,
  StyleSheet,
  StatusBar,
  View,
  ScrollView,
  Dimensions,
  Platform,
  Alert
} from 'react-native';
import { Button, Container, Content, Card, CardItem, Thumbnail, List, ListItem, InputGroup, Text, Input } from 'native-base';
import theme from '../../themes/base-theme';
import { COLORS } from '../../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import { Router } from '../../Router';
import Meteor, { createContainer } from 'react-native-meteor';

const styles = StyleSheet.create({
  maincontainer:{
    flex: 0,
    height:Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  map: {
    flex: 0,
    top: 0,
    height: Dimensions.get('window').height -112,
  },
  buttonsoverlay:{
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: -45,
    top: 0,
    width: 100,
    zIndex: 100,
    flex: 0,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor:COLORS.transparent,
  },
  mapbuttons:{
    width: 40,
    height: 40,
    borderRadius: 40/2,
    backgroundColor: COLORS.avatarBackground,
    alignItems:'center',
    justifyContent: 'center',
    marginRight: 2,
  },
  mapbuttonicons:{
    fontSize: 30,
    color: COLORS.sidebar,
  }
});

const accessToken = 'pk.eyJ1IjoiamF2YTk5IiwiYSI6ImNpaXg5Yms1czAwMGR1cm0wdWV2NmU1ZWQifQ.CFFZSrWcj1biDESmiuCNqQ';
Mapbox.setAccessToken(accessToken);

class Map extends Component {
  state = {
    center: this.props.passedLocation,
    zoom: 7,
    onLoadsetCenter: this.props.onLoadSetCenter,
    userTrackingMode: Mapbox.userTrackingMode.none,
    annotations: [],
    currentLocation: {
            latitude: 48.11052634,
            longitude: 11.0686958312988
            },
  };

  componentWillMount() {
        this._offlineProgressSubscription = Mapbox.addOfflinePackProgressListener(progress => {
          console.log('offline pack progress', progress);
        });
        this._offlineMaxTilesSubscription = Mapbox.addOfflineMaxAllowedTilesListener(tiles => {
          console.log('offline max allowed tiles', tiles);
        });
        this._offlineErrorSubscription = Mapbox.addOfflineErrorListener(error => {
          console.log('offline error', error);
        });
  }

  onRegionDidChange = (location) => {
    this.setState({ currentZoom: location.zoomLevel });
    // console.log('onRegionDidChange', location);
  };
  onRegionWillChange = (location) => {
    // console.log('onRegionWillChange', location);
  };
  onUpdateUserLocation = (location) => {
    this.setState({currentLocation: {latitude: location.latitude, longitude: location.longitude}})
    // if (this.state.userTrackingMode !== Mapbox.userTrackingMode.none)
    if (this.state.onLoadsetCenter) {
        this._map.setCenterCoordinate(location.latitude, location.longitude, animated = true);
      this.setState({onLoadsetCenter: false});
    }
    // console.log('onUpdateUserLocation', location);
  };
  onOpenAnnotation = (annotation) => {
    if (Platform.OS === 'android')
      this._map.setCenterCoordinate(annotation.latitude, annotation.longitude, animated = true);
    console.log('onOpenAnnotation', annotation);
  };
  onRightAnnotationTapped = (e) => {
    console.log('onRightAnnotationTapped', e);
    this.props.navigator.push(Router.getRoute('markerdetails', {fooditemId: e.id})); // pass marker id to chat = chatgroup ref
    // this.props.navigation.performAction(({ tabs, stacks }) => {
    //   tabs('main').jumpToTab('contacts');
    //   stacks('contacts').push(Router.getRoute('chat', {chatGroupId: e.id}));
    // });
  };
  onLongPress = (location) => {
    console.log('onLongPress', location);
  };
  onTap = (location) => {
    console.log('onTap', location);
  };
  onChangeUserTrackingMode = (userTrackingMode) => {
    console.log('onChangeUserTrackingMode', userTrackingMode);
  };

  componentWillReceiveProps(nextProps) {
    var fooditems = nextProps.fooditems;
    var annotationMarkers = [];
      fooditems.forEach(function(fooditem) {
        // Treat annotations as immutable and create a new one instead of using .push()
        if (fooditem.lat && fooditem.lng) {
          var annotationMarker = {
            coordinates: [fooditem.lat, fooditem.lng],
            type: 'point',
            title: fooditem.itemName,
            subtitle: "Click me for Details",
            id: fooditem._id,
            annotationImage: {
              source: { uri: 'marker' },
              height: 80,
              width: 40
            },
            rightCalloutAccessory: {
              source: { uri: 'https://cldup.com/9Lp0EaBw5s.png' },
              height: 25,
              width: 25
            }
          };
          annotationMarkers.push(annotationMarker);
        }
      }); // end for loop over food items

    // var aggregateMarkers = [];
    // // pre-existing markers
    //  aggregateMarkers.push(...this.state.annotations);
    //
    // // plus user markers
    // for (var i = 0; i < annotationMarkers.length; i++) {
    //   aggregateMarkers.push(annotationMarkers[i]);
    // }

    this.setState({
      annotations: annotationMarkers
    });
  }

  componentWillUnmount() {
    this._offlineProgressSubscription.remove();
    this._offlineMaxTilesSubscription.remove();
    this._offlineErrorSubscription.remove();
    // handle.stop();
  }

  render() {
    StatusBar.setHidden(true);
    // TODO Map should get WM3QC2A key for search - by passing as prop
    return (
      <Container>
        <Content theme={theme} style={styles.maincontainer}>
        {this._renderButtons()}
        <MapView
          ref={map => { this._map = map; }}
          style={styles.map}
          initialCenterCoordinate={this.state.center}
          initialZoomLevel={this.state.zoom}
          initialDirection={0}
          rotateEnabled={true}
          scrollEnabled={true}
          zoomEnabled={true}
          pitchEnabled={false}
          showsUserLocation={true}
          styleURL='mapbox://styles/java99/citmj4f4n00222ikccjk93lxj'
          annotations={this.state.annotations}
          annotationsAreImmutable={true}
          onChangeUserTrackingMode={this.onChangeUserTrackingMode}
          onRegionDidChange={this.onRegionDidChange}
          onRegionWillChange={this.onRegionWillChange}
          onOpenAnnotation={this.onOpenAnnotation}
          onRightAnnotationTapped={this.onRightAnnotationTapped}
          onUpdateUserLocation={this.onUpdateUserLocation}
          onLongPress={this.onLongPress}
          onTap={this.onTap}
          userTrackingMode={this.state.userTrackingMode}
        />
        </Content>
        </Container>
    );
  }
  toggleFollowing(){
    if (this.state.userTrackingMode == Mapbox.userTrackingMode.none) {
      // this._map.setT
      this.setState({ userTrackingMode: Mapbox.userTrackingMode.followWithHeading })
      Alert.alert('Live Mode On');
    }
    else {
      this.setState({ userTrackingMode: Mapbox.userTrackingMode.none })
      Alert.alert('Live Mode Off');
    }
  }


  _renderButtons() {
    return (
      <View style={styles.buttonsoverlay}>
        <Button style={styles.mapbuttons} onPress={() => this._map.setCenterCoordinate(this.state.currentLocation.latitude, this.state.currentLocation.longitude, animated = true)}>
        <Icon style={styles.mapbuttonicons} name="md-locate"/>
        </Button>
        <Button style={styles.mapbuttons} onPress={this.toggleFollowing.bind(this)}>
        {(this.state.userTrackingMode == Mapbox.userTrackingMode.none) ? (
          <Icon style={styles.mapbuttonicons} name="md-play"/>
        ) : (
          <Icon style={styles.mapbuttonicons} name="md-pause"/>
        )
       }
        </Button>
      </View>
    );
  }
}

Map.propTypes = {
  navigator: React.PropTypes.object,
  passedLocation: React.PropTypes.any,
  onLoadSetCenter: React.PropTypes.bool,
};


export default createContainer(({location}) => {
  var defaultLocation = { latitude: 48.11052634, longitude: 11.0686958312988 }
  var onLoadCenterMap = false;
  if (!location) {
  location = defaultLocation;
  onLoadCenterMap = true;
  }
  const handle = Meteor.subscribe('fooditems');
  return {
    itemsReady: handle.ready(),
    fooditems: Meteor.collection('fooditems').find(),
    passedLocation: location,
    onLoadSetCenter: onLoadCenterMap,
  };
}, Map);
