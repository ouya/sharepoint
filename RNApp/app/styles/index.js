import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';

const GRID_ITEMS_PER_ROW = 2;
const {width} = Dimensions.get('window');

const GRID_SETTINGS = {};
GRID_SETTINGS.margin = 5;
GRID_SETTINGS.width = (width - 2*GRID_ITEMS_PER_ROW*GRID_SETTINGS.margin)/GRID_ITEMS_PER_ROW;

export {GRID_SETTINGS};

export const COLORS = {
  transparent: 'rgba(0,0,0,0)',
  background: '#47ce3f',
  errorText: '#FA3256',
  headerText: '#EEEEEE',
  buttonBackground: '#a89ad5',
  avatarBackground: '#5b369d',
  buttonText: '#EEEEEE',
  inputBackground: '#EEEEEE',
  inputDisabled: '#777777',
  inputDisabledBackground: '#EEEEEE',
  inputBgBar: '#CCCCCC',
  inputDivider: '#E4E2E5',
  mainGreenColor: '#3ad111',
  secondGreenColor: '#d1b44e',
  mainPurpleColor: '#a89ad5',
  navigationBarTintColor: '#EEEEEE',
  navigationBarBackgroundColor: '#47ce3f',
  // navigationBarBackgroundColor: 'rgba(0,0,0,0)',
  tabBar: '#EEEEEE',
  tabBackgroundSelected: '#a89ad5',
  tabIconDefault: '#3ad111',
  tabIconSelected: '#5b369d',
  buttonIconColor: '#252932',
  //tabs for contacts page
  tabBgColor: '#EEEEEE',
  tabTextColor: '#3ad111',
  primary: '#47ce3f',
  secondary: '#a89ad5',
  info: '#FFFFFF',
  success: '#5cb85c',
  danger: '#d9534f',
  warning: '#f0ad4e',
  sidebar: '#FFFFFF',
  dark: '#5b369d',
  light: '#EEEEEE',
};

export const STYLES = {

};
