package eu.feedmyfriends;

import com.facebook.react.ReactActivity;
import com.evollu.react.fcm.FIRMessagingPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.mapbox.reactnativemapboxgl.ReactNativeMapboxGLPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.rnfs.RNFSPackage;

import android.content.Intent;

public class MainActivity extends ReactActivity {

     @Override
     public void onNewIntent (Intent intent) {
       super.onNewIntent(intent);
         setIntent(intent);
     }

    @Override
    protected String getMainComponentName() {
        return "RNApp";
    };
}
